package msc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.beans.Transient;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;

public class Diagram extends JComponent {

	private ArrayList<Node> nodes;
	private ArrayList<Message> msgs;
	private Title title;
	private Dimension dimension;

	public Diagram() {
		setOpaque(true);
		init();
	}

	public void setModel(DiagramModel model) {
		if (model != null) {
			this.nodes = model.getNodes();
			this.msgs = model.getMessages();
			this.title = new Title(model.getTitle());
		} else {
			this.nodes = null;
			this.msgs = null;
			this.title = null;
		}
		init();
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
		if (title != null) {
			title.setAvailableWidth(dimension.width);
		}
		revalidate();
	}
	protected void init() {
		if (nodes == null || nodes.size() == 0) {
			setDimension(new Dimension(600, 600));
			return;
		}

		int height = 50 + nodes.get(0).getNodeHeight() + 100;

		int y = 50;
		int x = 55;

		for (int i = 0; i < nodes.size(); i++) {
			Node n = nodes.get(i);

			x += n.getLeftLead(msgs);
			n.setLeftX(x);
			x += n.getRightLead(msgs);

			n.setLeftY(y);
		}

		// increase node to node width if needed
		for (Message message : msgs) {
			Node origin = message.getNodeOrigin();
			Node dest = message.getNodeDestination();
			int requestedWidth = message.getWidth();
			if (dest == null || origin == dest)
				continue;

			int availableWidth = dest.getCenterX(message)
					- origin.getCenterX(message);

			if (requestedWidth > Math.abs(availableWidth)) {
				// push leftmost node and its subs.
				if (availableWidth > 0) {
					pushNodesLeftX(dest.getNodePosition(), requestedWidth
							- availableWidth);
				} else {
					pushNodesLeftX(origin.getNodePosition(), requestedWidth
							+ availableWidth);
				}
			}
		}

		// add more space to nodes, if a message require mode space to render
		// properly
		int width = nodes.get(nodes.size() - 1).getLeftX()
				+ nodes.get(nodes.size() - 1).getRightLead() + 50;
		width = Math.max(600, width);
		setDimension(new Dimension(width, height));
	}

	/**
	 * @param startIndex
	 *            the node where the push starts
	 * @param offset
	 */
	protected void pushNodesLeftX(int startIndex, int offset) {
		for (int i = startIndex; i < nodes.size(); i++) {
			Node n = nodes.get(i);
			n.setLeftX(n.getLeftX() + offset);
		}
	}

	@Override
	@Transient
	public Dimension getMinimumSize() {
		return dimension;
	}

	@Override
	@Transient
	public Dimension getPreferredSize() {
		return dimension;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getSize().width, getSize().height);
		super.paint(g);
	}

	public void paintComponent(Graphics g1) {
		Graphics2D g = (Graphics2D) g1.create();
		super.paintComponent(g);
		if (nodes != null) {
			removeAll();
			for (int i = 0; i < nodes.size(); i++) {
				JLabel topLabel = nodes.get(i).getLabel();
				JLabel botLabel = nodes.get(i).getBotName();
				if (topLabel.getText().equals("1nP") || topLabel.getText().equals("2nP")) {
					continue;
				}
				nodes.get(i).paintComponent(g);
				add(topLabel);
				add(botLabel);
			}
		}

		if (msgs != null) {
			// paint messages
			for (int i = 0; i < msgs.size(); i++) {
				msgs.get(i).paintMessage(g);
			}
		}

		if (title != null)
			title.paintTitle(g);
	}
}
