package msc;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultDesktopManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.filechooser.FileFilter;

import fsm.FSMFrame;
public class Frame extends JFrame {

	private JDesktopPane desktopPane;
	private boolean textPressed = false; // for when "text" radio button is
											// clicked or not
	private boolean imagePressed = false; // for when "image" radio button is
											// clicked or not
	private static JTextArea textArea = new JTextArea();
	private Diagram diag;
	private Timer timer;
	private static TextArea errorArea;
	private DiagramModel diagModel;
	private static ArrayList<Node> allOpenNodes = new ArrayList<>();
	private JScrollPane scrollText = new JScrollPane(textArea);
	private JMenuBar menuBar;
	private InternalImmovableFrame mscFrame;
	private final ImageIcon internalFrameIcon = new ImageIcon(getClass().getResource("images/frameIcon.png"));

	public Frame(Diagram diag) {
		super("MSC/FSM Diagram Buddy");
		this.diag = diag;
		addComponentListener(new ComponentAdapter() {



			@Override
			public void componentResized(ComponentEvent e) {
				mscFrame.setSize(getSize().width - 15, getSize().height - 37);
			}
		});
		desktopPane = new JDesktopPane() {

			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
		};
		desktopPane.setDesktopManager(new ImmovableDesktopManager());
		setContentPane(desktopPane);
		mscFrame = new InternalImmovableFrame("MSC");
		javax.swing.plaf.InternalFrameUI ifu = mscFrame.getUI();
		((javax.swing.plaf.basic.BasicInternalFrameUI) ifu).setNorthPane(null);
		mscFrame.setBorder(null);
		desktopPane.add(mscFrame);
		menuBar = new JMenuBar();
		ImageIcon frameIcon = new ImageIcon(getClass().getResource("images/frameIcon.png"));
		JMenu fileMenu = new JMenu("File");
		textArea.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if (timer != null)
					timer.cancel();
				timer = new Timer();
				timer.schedule(new TimerTask() {

					@Override
					public void run() {
						parse();
					}
				}, 1000);
			}




			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
		// the text area
		ImageIcon iconNew = new ImageIcon(getClass().getResource("images/menuNew.png"));
		ImageIcon iconOpen = new ImageIcon(getClass().getResource("images/menuOpen.png"));
		ImageIcon iconSave = new ImageIcon(getClass().getResource("images/menuSave.png"));
		ImageIcon iconExit = new ImageIcon(getClass().getResource("images/menuExit.png"));
		ImageIcon saveProjectIcon = new ImageIcon(getClass().getResource("images/saveProjectIcon.png"));
		ImageIcon openProjectIcon = new ImageIcon(getClass().getResource("images/openProjectIcon.png"));
		JMenuItem menuItemNew = new JMenuItem("New", iconNew);
		JMenuItem menuItemOpen = new JMenuItem("Open MSC..", iconOpen);
		JMenuItem menuItemSave = new JMenuItem("Save MSC..", iconSave);
		JMenuItem menuItemExit = new JMenuItem("Exit", iconExit);
		JMenuItem menuItemSaveProject = new JMenuItem("Save Project..", saveProjectIcon);
		JMenuItem menuItemOpenProject = new JMenuItem("Open Project..", openProjectIcon);
		// final JMenuItem menuItemMSC = new JMenuItem("MSC Diagram");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuItemNew.setMnemonic(KeyEvent.VK_N);
		menuItemNew.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the data? Any unsaved changes will be lost!", "New MSC", JOptionPane.YES_NO_OPTION);
				if (optionChosen == JOptionPane.YES_OPTION) {
					textArea.setText(null);
					removeInternalFrames();
				}
			}
		});
		menuItemOpen.setMnemonic(KeyEvent.VK_O);
		menuItemOpen.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemOpen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileFilter() {

					@Override
					public String getDescription() {
						return "TXT File (*.txt)";
					}


					@Override
					public boolean accept(File file) {
						return (file.getName().toLowerCase().endsWith(".txt") || file.isDirectory());
					}
				};
				fileChooser.setFileFilter(filter);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int val = fileChooser.showOpenDialog(null);
				if (val == JFileChooser.APPROVE_OPTION) {
					int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to close the existing diagram? Any unsaved changes will be lost!", "Open MSC",
							JOptionPane.YES_NO_OPTION);
					if (optionChosen == JOptionPane.YES_OPTION) {
						File file = fileChooser.getSelectedFile();
						try {
							textArea.read(new FileReader(file.getAbsolutePath()), null);
						} catch (IOException ex) {
							errorArea.setText("Problem accessing file!" + file.getAbsolutePath());
						}
					}
				}
			}
		});
		menuItemOpenProject.setMnemonic(KeyEvent.VK_L);
		menuItemOpenProject.setAccelerator(KeyStroke.getKeyStroke('L', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemOpenProject.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileFilter() {

					@Override
					public String getDescription() {
						return "XML File (*.xml)";
					}

					@Override
					public boolean accept(File file) {
						return (file.getName().toLowerCase().endsWith(".xml") || file.isDirectory());
					}
				};
				fileChooser.setFileFilter(filter);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int val = fileChooser.showOpenDialog(null);
				if (val == JFileChooser.APPROVE_OPTION) {
					int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to close the existing project? Any unsaved changes will be lost!", "Open Project",
							JOptionPane.YES_NO_OPTION);
					if (optionChosen == JOptionPane.YES_OPTION) {
						File file = fileChooser.getSelectedFile();
						XMLtoProject xmlToProject = new XMLtoProject(file.getAbsolutePath());
						textArea.setText(xmlToProject.getMscText());
						parse();
						constructProject(xmlToProject.getFsmFrames());
					}
				}
			}
		});
		menuItemSave.setMnemonic(KeyEvent.VK_S);
		menuItemSave.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemSave.addActionListener(new SaveMscListener());
		menuItemSaveProject.setMnemonic(KeyEvent.VK_P);
		menuItemSaveProject.setAccelerator(KeyStroke.getKeyStroke('P', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemSaveProject.addActionListener(new SaveProjectListener());
		menuItemExit.setMnemonic(KeyEvent.VK_X);
		menuItemExit.setAccelerator(KeyStroke.getKeyStroke('X', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit the program? Any unsaved changes will be lost!", "Exit", JOptionPane.YES_NO_OPTION);
				if (optionChosen == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
		JSeparator separator = new JSeparator();
		separator.setVisible(false);
		menuItemExit.add(separator);
		fileMenu.add(menuItemNew);
		fileMenu.add(menuItemOpen);
		fileMenu.add(menuItemOpenProject);
		fileMenu.add(menuItemSave);
		fileMenu.add(menuItemSaveProject);
		fileMenu.add(menuItemExit);
		menuBar.add(fileMenu);
		GridBagLayout bagLayout = new GridBagLayout();
		mscFrame.setLayout(bagLayout);
		JScrollPane diagPane = new JScrollPane(this.diag, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		textArea.setFont(new Font(null, Font.PLAIN, 12));
		errorArea = new TextArea("No errors.");
		errorArea.setFont(new Font(null, Font.PLAIN, 12));
		errorArea.setEditable(false);
		Color imbaRed = new Color(168, 0, 54);
		errorArea.setForeground(imbaRed);
		errorArea.setBackground(Color.white);
		diagPane.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 3;
		c.gridwidth = 1;
		c.weightx = 0.0;
		c.weighty = 0.1;
		scrollText.setPreferredSize(new Dimension(220, 200));
		scrollText.setMaximumSize(new Dimension(220, 200));
		scrollText.setMinimumSize(new Dimension(220, 200));
		mscFrame.add(scrollText, c);
		c.gridx = 0;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.0;
		c.weighty = 0.0;
		errorArea.setPreferredSize(new Dimension(220, 100));
		errorArea.setMaximumSize(new Dimension(220, 100));
		errorArea.setMinimumSize(new Dimension(220, 100));
		mscFrame.add(errorArea, c);
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 4;
		c.gridwidth = 4;
		c.weightx = 1;
		c.weighty = 1;
		mscFrame.add(diagPane, c);
		mscFrame.setJMenuBar(menuBar);
		setSize(1000, 800);
		desktopPane.setSize(getSize());
		UIManager.put("DesktopIcon.width", 200);
		mscFrame.setVisible(true);
		setLocationRelativeTo(null);
		setVisible(true);
		setIconImage(frameIcon.getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private static class InternalImmovableFrame extends JInternalFrame {

		public InternalImmovableFrame(String title) {
			super(title, false, false, false, false);
		}
	}

	private static class ImmovableDesktopManager extends DefaultDesktopManager {

		public void dragFrame(JComponent f, int x, int y) {
			if (!(f instanceof InternalImmovableFrame)) {
				super.dragFrame(f, x, y);
			}
		}
	}

	private void paintDiagram(JInternalFrame fr) {
		JInternalFrame fsmFrame = fr;
		if (fsmFrame.getTitle().equals("MSC")) {
			return;
		}
		String frameName = fsmFrame.getTitle().substring(4);
		int pos = 1;
		Node selectedNode = DiagramParser.getNode(diagModel.getNodes(), frameName);
		Node centerNode = new Node(selectedNode.getName(), 1);
		Node leftNode = new Node("1nP", 0);
		Node rightNode = new Node("2nP", 2);
		centerNode.setNodeId(selectedNode.getNodeId());
		centerNode.setNodeHeight(selectedNode.getNodeHeight());
		leftNode.setNodeHeight(selectedNode.getNodeHeight());
		centerNode.setActRegion(selectedNode.getActRegion());
		boolean nodeNameExists = false;
		for (Node node : allOpenNodes) {
			if (node.getName().equals(selectedNode.getName())) {
				nodeNameExists = true;
				break;
			}
		}
		if (!nodeNameExists) {
			allOpenNodes.add(centerNode);
		}
		ArrayList<Node> newNodes = new ArrayList<>();
		newNodes.add(leftNode);
		newNodes.add(centerNode);
		newNodes.add(rightNode);
		ArrayList<Message> messages = diagModel.getMessages();
		ArrayList<Message> newMessages = new ArrayList<>();
		for (int index = 0; index < messages.size(); index++) {
			if (messages.get(index) instanceof Signal) {
				if ((messages.get(index).getNodeOrigin().getName()).equals(frameName)) {
					Signal signal = (Signal) messages.get(index);
					if (selectedNode.getNodePosition() > messages.get(index).getNodeDestination().getNodePosition()) {
						Signal newSignal = new Signal(centerNode, leftNode, signal.getName(), pos, signal.getKind());
						newMessages.add(newSignal);
						pos++;
					} else {
						if (selectedNode.getNodePosition() < messages.get(index).getNodeDestination().getNodePosition()) {
							Signal newSignal = new Signal(centerNode, rightNode, signal.getName(), pos, signal.getKind());
							newMessages.add(newSignal);
							pos++;
						} else {
							Signal newSignal = new Signal(centerNode, centerNode, signal.getName(), pos, signal.getKind());
							newMessages.add(newSignal);
							pos++;
						}
					}
				}
				if ((messages.get(index).getNodeDestination().getName()).equals(frameName)) {
					Signal signal = (Signal) messages.get(index);
					if (selectedNode.getNodePosition() > messages.get(index).getNodeOrigin().getNodePosition()) {
						Signal newSignal = new Signal(leftNode, centerNode, signal.getName(), pos, signal.getKind());
						newMessages.add(newSignal);
						pos++;
					} else {
						if (selectedNode.getNodePosition() < messages.get(index).getNodeOrigin().getNodePosition()) {
							Signal newSignal = new Signal(rightNode, centerNode, signal.getName(), pos, signal.getKind());
							newMessages.add(newSignal);
							pos++;
						}
					}
				}
			}
		}
		String title = "";
		FSMFrame fsmFr = (FSMFrame) fsmFrame;
		fsmFr.getDiag().setModel(new DiagramModel(newNodes, newMessages, title));
		fsmFr.getDiag().repaint();
	}

	private void parse() {
		System.out.println("Will parse text...");
		DiagramModel model = null;
		String error = "";
		try {
			model = new DiagramParser().parse(textArea.getText());
		} catch (Exception e) {
			e.printStackTrace();
			error = "Syntax error";
			errorArea.setText(error + "!");
		}
		diag.setModel(model);
		System.out.println(diag.getPreferredSize());
		diagModel = model;
		addListenerToNodes();
		if (desktopPane.getAllFrames().length > 1) {
			adjustInternalFrames();
		}
		errorArea.setText(error);
		this.repaint();
	}

	private void addListenerToNodes() {
		ArrayList<Node> nodes = diagModel.getNodes();
		if (!nodes.isEmpty() || nodes != null) {
			for (final Node node : nodes) {
				node.getLabel().addMouseListener(new MouseAdapter() {

					public void mouseClicked(MouseEvent e) {
						addInternalFrame(node.getName());
					}
				});
			}
		}
	}

	private void adjustInternalFrames() {
		ArrayList<Node> mscNodes = diagModel.getNodes();
		if (mscNodes.isEmpty()) {
			return;
		}
		boolean nodeNameExists = false;
		boolean nodePositionExists = false;
		for (int index = 0; index < allOpenNodes.size(); index++) {
			nodeNameExists = checkIfNodeNameExists(allOpenNodes.get(index), mscNodes);
			if (nodeNameExists) {
				continue;
			}
			nodePositionExists = checkIfNodeIdExists(allOpenNodes.get(index), mscNodes);


			if (nodePositionExists) {
				continue;
			}
			for (JInternalFrame intFr : desktopPane.getAllFrames()) {
				if (intFr.getTitle().equals("FSM " + allOpenNodes.get(index).getName())) {
					intFr.dispose();
					break;
				}
			}
			allOpenNodes.remove(index);
			index--;
		}
	}

	private boolean checkIfNodeNameExists(Node nodeToCheck, ArrayList<Node> nodes) {
		for (Node node : nodes) {
			if (nodeToCheck.getName().equals(node.getName())) {
				return true;
			}
		}
		return false;
	}

	private boolean checkIfNodeIdExists(Node nodeToCheck, ArrayList<Node> nodes) {
		for (Node node : nodes) {
			if (nodeToCheck.getNodeId() == node.getNodeId()) {
				changeInternalFrameName(nodeToCheck.getName(), node.getName());
				nodeToCheck.setName(node.getName());
				return true;
			}
		}
		return false;
	}

	private void changeInternalFrameName(String oldName, String newName) {
		for (JInternalFrame intFr : desktopPane.getAllFrames()) {
			if (intFr.getTitle().equals("FSM " + oldName)) {
				intFr.setTitle("FSM " + newName);
				break;
			}
		}
	}

	class SaveProjectListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			FileFilter filter = new FileFilter() {

				@Override
				public String getDescription() {
					return "XML File (*.xml)";
				}


				@Override
				public boolean accept(File file) {
					return (file.getName().toLowerCase().endsWith(".xml") || file.isDirectory());
				}
			};
			fileChooser.setFileFilter(filter);
			fileChooser.setAcceptAllFileFilterUsed(false);
			int val = fileChooser.showSaveDialog(null);
			if (val == JFileChooser.APPROVE_OPTION) {
				try {
					File file = new File(fileChooser.getSelectedFile().getPath());
					if (!fileChooser.getSelectedFile().getName().endsWith(".xml")) {
						file = new File(fileChooser.getSelectedFile() + ".xml");
					}
					if (file.exists()) {
						val = JOptionPane.showConfirmDialog(fileChooser, "Replace existing file?");
						if (val == JOptionPane.CLOSED_OPTION) {
							return;
						} else if (val == JOptionPane.YES_OPTION) {
							new ProjectToXML(textArea.getText(), desktopPane, file.getAbsolutePath());
							JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
						}
					} else {
						new ProjectToXML(textArea.getText(), desktopPane, file.getAbsolutePath());
						JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception s) {
					errorArea.setText("Error: " + s.getMessage());
				}
			}
		}
	}



	class SaveMscListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			final JDialog dialog = new JDialog();
			ImageIcon frameIcon = new ImageIcon(getClass().getResource("images/menuSave.png"));
			dialog.setIconImage(frameIcon.getImage());
			GridBagLayout layout = new GridBagLayout();
			GridBagConstraints c = new GridBagConstraints();
			final JButton ok = new JButton("OK");
			ok.setEnabled(false);
			ok.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (textPressed == true) {
						JFileChooser fileChooser = new JFileChooser();
						FileFilter filter = new FileFilter() {

							@Override
							public String getDescription() {
								return "Text File (*.txt)";
							}

							@Override
							public boolean accept(File file) {
								return (file.getName().toLowerCase().endsWith(".txt") || file.isDirectory());
							}
						};
						fileChooser.setFileFilter(filter);
						fileChooser.setAcceptAllFileFilterUsed(false);
						int val = fileChooser.showSaveDialog(null);
						if (val == JFileChooser.APPROVE_OPTION) {
							try {
								File file = new File(fileChooser.getSelectedFile().getPath());
								if (!fileChooser.getSelectedFile().getName().endsWith(".txt")) {
									file = new File(fileChooser.getSelectedFile() + ".txt");
								}
								if (file.exists()) {
									val = JOptionPane.showConfirmDialog(fileChooser, "Replace existing file?");
									if (val == JOptionPane.CLOSED_OPTION) {
										return;
									} else if (val == JOptionPane.YES_OPTION) {
										FileWriter fstream = new FileWriter(file);
										BufferedWriter out = new BufferedWriter(fstream);
										out.write(textArea.getText());
										out.close();
										JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
										dialog.dispose();
									}
								} else {
									FileWriter fstream = new FileWriter(file);
									BufferedWriter out = new BufferedWriter(fstream);
									out.write(textArea.getText());
									out.close();
									JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
									dialog.dispose();
								}
							} catch (Exception s) {
								errorArea.setText("Error: " + s.getMessage());
							}
						}
					} else if (imagePressed == true) {
						JFileChooser fileChooser = new JFileChooser();
						FileFilter filter = new FileFilter() {

							@Override
							public String getDescription() {
								return "PNG File (*.png)";
							}

							@Override
							public boolean accept(File file) {
								return (file.getName().toLowerCase().endsWith(".png") || file.isDirectory());
							}
						};
						fileChooser.setFileFilter(filter);
						fileChooser.setAcceptAllFileFilterUsed(false);
						int val = fileChooser.showSaveDialog(null);
						if (val == JFileChooser.APPROVE_OPTION) {
							try {
								File file = new File(fileChooser.getSelectedFile().getPath());
								if (!fileChooser.getSelectedFile().getName().endsWith(".png")) {
									file = new File(fileChooser.getSelectedFile() + ".png");
								}
								if (file.exists()) {
									val = JOptionPane.showConfirmDialog(fileChooser, "Replace existing file?");
									if (val == JOptionPane.CLOSED_OPTION) {
										return;
									} else if (val == JOptionPane.YES_OPTION) {
										BufferedImage bi = new BufferedImage(diag.getPreferredSize().width, diag.getPreferredSize().height, BufferedImage.TYPE_INT_RGB);
										Graphics g = bi.createGraphics();
										g.setColor(Color.WHITE);
										diag.paint(g);
										ImageIO.write(bi, "png", file);
										JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
										dialog.dispose();
									}
								} else {
									BufferedImage bi = new BufferedImage(diag.getPreferredSize().width, diag.getPreferredSize().height, BufferedImage.TYPE_INT_RGB);
									Graphics g = bi.createGraphics();
									g.setColor(Color.WHITE);
									diag.paint(g);
									ImageIO.write(bi, "png", file);
									JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
									dialog.dispose();
								}
							} catch (Exception s) {
								System.err.println("Error: " + s.getMessage());
							}
						}
					}
				}
			});
			JButton cancel = new JButton("Cancel");
			cancel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() instanceof JButton) {
						Container c = ((JButton) (e.getSource())).getParent();
						while ((c.getParent() != null) && (!(c instanceof JDialog))) {
							c = c.getParent();
						}
						if (c instanceof JDialog) {
							JDialog d = (JDialog) c;
							d.dispose();
						}
					}
				}
			});
			JPanel panel = new JPanel(layout);
			panel.setOpaque(true);
			panel.setBackground(Color.WHITE);
			ButtonGroup bg = new ButtonGroup();
			dialog.setTitle("Save MSC..");
			JRadioButton text = new JRadioButton("Source Text");
			text.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ok.setEnabled(true);
					textPressed = true;
					imagePressed = false;
				}
			});
			final JRadioButton image = new JRadioButton("PNG Image");
			image.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ok.setEnabled(true);
					imagePressed = true;
					textPressed = false;
				}
			});
			text.setBackground(Color.white);
			image.setBackground(Color.white);
			bg.add(text);
			bg.add(image);
			c.fill = GridBagConstraints.CENTER;
			c.insets = new Insets(40, 0, 0, 0);
			c.gridx = 0;
			c.gridy = 0;
			panel.add(text, c);
			c.insets = new Insets(40, 0, 0, 0);
			c.gridx = 1;
			c.gridy = 0;
			panel.add(image, c);
			c.ipadx = 40;
			c.weighty = 1;
			c.anchor = GridBagConstraints.SOUTH;
			c.insets = new Insets(0, 20, 20, 20);
			c.gridx = 0;
			c.gridy = 4;
			panel.add(ok, c);
			c.ipadx = 20;
			c.gridx = 1;
			c.gridy = 4;
			panel.add(cancel, c);
			dialog.add(panel);
			dialog.setResizable(false);
			dialog.setSize(400, 200);
			dialog.setLocationRelativeTo(null);
			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			dialog.setModal(true);
			dialog.setVisible(true);
		}
	}


	private void addInternalFrame(final String name) {
		FSMFrame fsmFrame = null;
		boolean nameExists = false;
		if (desktopPane.getAllFrames().length == 1) {
			fsmFrame = new FSMFrame("FSM " + name, new Diagram());
			fsmFrame.setFrameIcon(internalFrameIcon);
			addListener(fsmFrame);
			fsmFrame.pack();
			desktopPane.add(fsmFrame, JLayeredPane.DRAG_LAYER);
			try {
				fsmFrame.setSelected(true);
			} catch (java.beans.PropertyVetoException e) {
			}
		} else {
			for (JInternalFrame internalFrame : desktopPane.getAllFrames()) {
				if (internalFrame.getTitle().equals("FSM " + name)) {
					nameExists = true;
				}
			}
			if (!nameExists) {
				fsmFrame = new FSMFrame("FSM " + name, new Diagram());
				fsmFrame.setFrameIcon(internalFrameIcon);
				addListener(fsmFrame);
				fsmFrame.pack();
				desktopPane.add(fsmFrame, JLayeredPane.DRAG_LAYER);
				try {
					fsmFrame.setSelected(true);
				} catch (java.beans.PropertyVetoException e) {
				}
			}
		}
	}

	private void removeInternalFrames() {
		removeAllInternalFrames();
		DiagramModel model = new DiagramModel(new ArrayList<Node>(), new ArrayList<Message>(), "");
		diag.setModel(model);
		this.repaint();
	}

	private void removeAllInternalFrames() {
		for (int i = desktopPane.getAllFrames().length - 2; i >= 0; i--) {
			JInternalFrame[] intFrs = desktopPane.getAllFrames();
			JInternalFrame intFr = intFrs[i];
			intFr.dispose();
		}
	}

	private void constructProject(ArrayList<FSMFrame> fsmFrames) {
		removeAllInternalFrames();
		for (FSMFrame fsmFrame : fsmFrames) {
			fsmFrame.setFrameIcon(internalFrameIcon);
			addListener(fsmFrame);
			fsmFrame.pack();
			desktopPane.add(fsmFrame, JLayeredPane.DRAG_LAYER);
			try {
				fsmFrame.setSelected(true);
				fsmFrame.setIcon(true);
			} catch (java.beans.PropertyVetoException e) {
			}
		}
	}

	private void addListener(JInternalFrame fr) {
		fr.addInternalFrameListener(new InternalFrameListener() {

			@Override
			public void internalFrameOpened(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameIconified(InternalFrameEvent e) {
			}


			@Override
			public void internalFrameDeiconified(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameDeactivated(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
			}

			@Override
			public void internalFrameActivated(InternalFrameEvent e) {
				paintDiagram(e.getInternalFrame());
			}
		});
	}
}
