package msc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JDesktopPane;

import fsm.FSMFrame;
import fsm.FSMtoXML;



public class ProjectToXML {

	private JDesktopPane desktopPane;
	private String mscText = "";
	private String result = "";
	private String filePath = "";

	public ProjectToXML(String mscText, JDesktopPane desktop, String filePath) {
		this.desktopPane = desktop;
		this.mscText = mscText;
		this.filePath = filePath;
		convertToXML();
		writeToFile();
		System.out.println(result);
	}

	private void convertToXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version='1.0' encoding='UTF-8'?>\r\n");
		sb.append("<project>\r\n");
		sb.append("\t<msc>" + mscText + "</msc>\r\n");
		for (int index = 0; index < desktopPane.getAllFrames().length - 1; index++) {
			FSMFrame fsmFrame = (FSMFrame) desktopPane.getAllFrames()[index];
			String fsmXML = new FSMtoXML(fsmFrame.getFSMDiagram().getStates(), fsmFrame.getFSMDiagram().getConnections(), fsmFrame.getTitle(), "").getResult();
			int startIndexOfFsm = fsmXML.indexOf("\t<fsmDiagram name = ");
			result = fsmXML.substring(startIndexOfFsm);
			sb.append(result);
		}
		sb.append("</project>");
		result = sb.toString();
	}

	private void writeToFile() {
		File projectFile = new File(filePath);
		try (FileWriter fileWriter = new FileWriter(projectFile); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
			if (!result.equals("")) {
				bufferedWriter.write(result);
			}
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	public String getResult() {
		return result;
	}
}
