package msc;

import java.util.ArrayList;
public class DiagramModel {

	private ArrayList<Node> nodes;
	private ArrayList<Message> messages;
	private String title;

	public DiagramModel(ArrayList<Node> nodes, ArrayList<Message> messages,
			String title) {
		this.nodes = nodes;
		this.messages = messages;
		this.title = title;
	}

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
