package msc;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;

public class Node extends JComponent {

	private class Region {

		int start;
		int end;

		public Region(int start, int end) {
			this.start = start;
			this.end = end;
		}

		public int getStart() {
			return start;
		}

		public int getEnd() {
			return end;
		}
	}

	private JLabel name = new JLabel();
	private JLabel botName = new JLabel();
	private int position;
	private int nodeId;
	private int leftX; // left X position of the node
	private int leftY; // left Y position of the node
	private ArrayList<Region> actRegion = new ArrayList<Region>();
	// keep last known activated slot, -1 otherwise
	private int activatedAtSlot = -1;
	private Font font;
	private FontMetrics fontMetrics;
	private int nodeHeight;
	private int nameTextPadding;
	private int nameTextWidth;
	private int nameTextHeight;
	private int nameRectWidth;
	private int nameRectHeight;

	public Node(String name, int position) {
		this.name.setText(name);
		this.botName.setText(name);
		this.name.setFont(new Font(null, Font.BOLD, 12));
		this.botName.setFont(new Font(null, Font.BOLD, 12));
		this.position = position;
		this.nodeId = position;
		init();
	}

	protected void init() {
		font = new Font(null, Font.PLAIN, 13);
		fontMetrics = getFontMetrics(font);
		nameTextPadding = 10;
		nameTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		nameTextHeight = fontMetrics.getHeight() - (nameTextPadding / 2);
		nameRectWidth = nameTextWidth + 2 * nameTextPadding;
		nameRectHeight = nameTextHeight + 2 * nameTextPadding;
	}

	public String getName() {
		return name.getText();
	}
	
	public void setName(String name) {
		this.name.setText(name);
	}

	public JLabel getLabel() {
		return name;
	}

	public JLabel getBotName() {
		return botName;
	}

	public int getNodeHeight() {
		return nodeHeight;
	}

	public void setNodeHeight(int height) {
		this.nodeHeight = height;
	}

	public void setMessageCount(int count) {
		this.nodeHeight = 50 + (count) * 55;
	}

	public int getNodePosition() {
		return position;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public int getLeftX() {
		return leftX;
	}

	public void setLeftX(int leftX) {
		this.leftX = leftX;
	}

	public int getLeftY() {
		return leftY;
	}

	public void setLeftY(int leftY) {
		this.leftY = leftY;
	}

	/**
	 * @return message left lead that is required in order to be rendered
	 *         properly
	 */
	public int getLeftLead() {
		return 0;
	}

	/**
	 * @param messages
	 *            to check if any start or end to this node
	 * @return message left lead that is required in order to be rendered
	 *         properly
	 */
	public int getLeftLead(ArrayList<Message> messages) {
		int lead = 0;


		for (Message message : messages) {
			if (message.getNodeOrigin() == this || message.getNodeDestination() == this)
				lead = Math.max(lead, (message.getLeftLead()));
		}
		return Math.max(lead, getLeftLead());
	}

	/**
	 * @return message right lead that is required in order to be rendered
	 *         properly
	 */
	public int getRightLead() {
		int lead = nameRectWidth + 10;
		return Math.max(30, lead);
	}

	/**
	 * @param messages
	 *            to check if any start or end to this node
	 * @return message right lead that is required in order to be rendered
	 *         properly
	 */
	public int getRightLead(ArrayList<Message> messages) {
		int lead = 0;
		for (Message message : messages) {
			if (message.getNodeOrigin() == this || message.getNodeDestination() == this)
				lead = Math.max(lead, (message.getRightLead()));
		}
		return Math.max(lead, getRightLead());
	}

	/**
	 * Message X
	 * 
	 * @param message
	 * @return
	 */
	public int getCenterX(Message message) {
		int nodeCenterX = leftX + nameRectWidth / 2;
		if (message != null && (isActive(message.getPosition() - 1) || isActive(message.getPosition() - 2))) {
			// message is active, we need to calc left | right corner
			int sign = 1;
			if (message.getNodeDestination() != null) {
				if (message.getNodeDestination().getCenterX(null) - message.getNodeOrigin().getCenterX(null) < 0) {
					sign = -1;
				}
			}
			if (message.getNodeDestination() != null && message.getNodeDestination().getNodePosition() == getNodePosition()) {
				sign *= -1;
			}
			nodeCenterX += 5 * sign;
		}
		return nodeCenterX;
	}

	/**
	 * Activate Node
	 * 
	 * @param startPos
	 *            Start slot of an active region.
	 * @param endPos
	 *            End slot of an active region.
	 */
	public void setActiveRegion(int startPos, int endPos) {
		actRegion.add(new Region(startPos, Math.max(startPos, endPos - 1)));
	}

	public boolean isActive(int slot) {
		for (Region region : actRegion) {
			if (region.getStart() <= slot && slot <= region.getEnd())
				return true;
		}
		return false;
	}

	public void activateAt(int slot) {
		activatedAtSlot = slot;
	}

	public void deactivateAt(int slot) {
		if (activatedAtSlot != -1) {
			setActiveRegion(activatedAtSlot, slot);
			activatedAtSlot = -1;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		int x = leftX;
		int y = leftY;
		g.setFont(font);
		// draw top node rect and text
		Color imbaYellow = new Color(255, 255, 207);
		Color imbaRed = new Color(168, 0, 54);
		g.setColor(imbaYellow);
		g.fillRect(x, y, nameRectWidth, nameRectHeight);
		g.setColor(imbaRed);
		g.drawRect(x, y, nameRectWidth, nameRectHeight);
		g.setColor(Color.BLACK);
		int nameX = x + (nameRectWidth - nameTextWidth) / 2;
		int nameY = y + nameTextPadding + nameTextHeight;
		name.setLocation(nameX, nameY - nameTextHeight);
		name.setSize(nameRectWidth, 15);
		// draw bottom node rect and text
		y += getNodeHeight() + nameRectHeight;
		System.out.println("1Y: = " + y + ", NH = " + getNodeHeight() + ", NR: " + nameRectHeight);
		g.setColor(imbaYellow);
		g.fillRect(x, y, nameRectWidth, nameRectHeight);
		g.setColor(imbaRed);
		g.drawRect(x, y, nameRectWidth, nameRectHeight);
		g.setColor(Color.BLACK);
		nameX = x + (nameRectWidth - nameTextWidth) / 2;
		nameY = y + nameTextPadding + nameTextHeight;
		botName.setLocation(nameX, nameY - nameTextHeight);
		botName.setSize(nameRectWidth, 15);
		// draw top <-> bottom line
		int lineX = x + nameRectWidth / 2;
		int lineY = y;
		g.setColor(imbaRed);
		g.drawLine(lineX, lineY - getNodeHeight(), lineX, lineY);
		g.setColor(Color.BLACK);
		// draw active region
		for (int i = 0; i < actRegion.size(); i++) {
			int startSlot = actRegion.get(i).getStart();
			int endSlot = actRegion.get(i).getEnd();
			int distance = 55;
			g.setColor(Color.white);
			g.fillRect(getCenterX(null) - 5, 110 + startSlot * distance, 10, (endSlot - startSlot + 1) * distance);
			g.setColor(imbaRed);
			g.drawRect(getCenterX(null) - 5, 110 + startSlot * distance, 10, (endSlot - startSlot + 1) * distance);
		}
		g.setColor(Color.BLACK);
	}

	public void setActRegion(ArrayList<Region> actRegion) {
		this.actRegion = actRegion;
	}

	public ArrayList<Region> getActRegion() {
		return actRegion;
	}
}
