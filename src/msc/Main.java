package msc;

import javax.swing.UIManager;

public class Main {

	public static void main(String[] args) {
		Diagram diag = new Diagram();
		try {
    	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    	} catch(Exception e) {
    	    System.err.println("Error setting native LAF: " + e);
    	} 
		new Frame(diag);
	}
}
