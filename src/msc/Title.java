package msc;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class Title {

	private String title;
	private int availableWidth;

	public Title(String title) {
		this.title = title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAvailableWidth(int width) {
		this.availableWidth = width;
	}

	public void paintTitle(Graphics g) {
		g.setFont(new Font(null, Font.BOLD, 17));
		FontMetrics fontMetrics = g.getFontMetrics();
		int x = 0;
		int y = 0;
		int titleTextWidth = fontMetrics.charsWidth(title.toCharArray(), 0,
				title.length());
		int titleTextHeight = fontMetrics.getMaxAscent();
		int titleTextPadding = (50 - titleTextHeight) / 2;

		y = titleTextHeight + titleTextPadding - 3;
		x = availableWidth / 2 - titleTextWidth / 2;

		g.drawString(title, x, y);
	}
}
