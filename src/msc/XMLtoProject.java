package msc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import fsm.Connection;
import fsm.FSMFrame;
import fsm.State;
import fsm.XMLtoFSM;

public class XMLtoProject {

	private String filePath = "";
	private String fileContents = "";
	private String mscText = "";
	private ArrayList<FSMFrame> fsmFrames = new ArrayList<>();

	public XMLtoProject(String filePath) {
		this.filePath = filePath;
		generateStringFromFile();
		extractMscText();
		extractFsmDiagrams();
	}

	private void generateStringFromFile() {
		try (Scanner fileReader = new Scanner(new File(filePath))) {
			StringBuilder sb = new StringBuilder();
			while (fileReader.hasNextLine()) {
				sb.append(fileReader.nextLine() + "\n");
			}
			fileContents = sb.toString();
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	private void extractMscText() {
		int startIndex = fileContents.indexOf("<msc>") + 5;
		int endIndex = fileContents.indexOf("</msc>");
		mscText = fileContents.substring(startIndex, endIndex);
	}

	private void extractFsmDiagrams() {
		int startIndexOfName = fileContents.indexOf("<fsmDiagram name = ") + 19;
		int endIndexOfName = fileContents.indexOf(">", startIndexOfName - 1);
		int startSearchIndex = fileContents.indexOf("<fsmDiagram name = ");
		int endSearchIndex = fileContents.indexOf("</fsmDiagram name = ", startSearchIndex);
		while (startSearchIndex >= 0) {
			String fsmName = fileContents.substring(startIndexOfName, endIndexOfName);
			XMLtoFSM xmlToFsm = new XMLtoFSM(fileContents, startSearchIndex, endSearchIndex);
			ArrayList<State> states = xmlToFsm.getStates();
			ArrayList<Connection> connections = xmlToFsm.getConnections();
			FSMFrame fsmFrame = new FSMFrame(fsmName, new Diagram());
			fsmFrame.getFSMDiagram().setStates(states);
			fsmFrame.getFSMDiagram().setConnections(connections);
			fsmFrames.add(fsmFrame);
			startIndexOfName = fileContents.indexOf("<fsmDiagram name = ", endIndexOfName) + 19;
			endIndexOfName = fileContents.indexOf(">", startIndexOfName - 1);
			startSearchIndex = fileContents.indexOf("<fsmDiagram name = ", endSearchIndex);
			endSearchIndex = fileContents.indexOf("</fsmDiagram name = ", startSearchIndex);
		}
	}

	public String getMscText() {
		return mscText;
	}

	public ArrayList<FSMFrame> getFsmFrames() {
		return fsmFrames;
	}
}
