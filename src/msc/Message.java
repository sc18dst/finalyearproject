package msc;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JComponent;

public abstract class Message extends JComponent {

	private Node nodeOrigin;
	private Node nodeDestination;
	private String name;
	private int position;

	protected Font font;
	protected FontMetrics fontMetrics;

	public Message(Node nodeOrigin, Node nodeDestination, String name,
			int position) {
		this.nodeOrigin = nodeOrigin;
		this.nodeDestination = nodeDestination;
		this.name = name;
		this.position = position;
	}

	protected void init() {
		font = new Font(null, Font.PLAIN, 12);
		fontMetrics = getFontMetrics(font);
	}

	public Node getNodeOrigin() {
		return nodeOrigin;
	}

	public Node getNodeDestination() {
		return nodeDestination;
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}
	public void setPosition(int pos) {
		this.position = pos;
	}

	public int getBaselineY() {
		return 110 + (position - 1) * 55;
	}

	/**
	 * @return the width of the message, required to be rendered properly
	 */
	public abstract int getWidth();

	/**
	 * @return message left lead that is required in order to be rendered
	 */
	public abstract int getLeftLead();

	/**
	 * @return message right lead that is required in order to be rendered
	 */
	public abstract int getRightLead();

	/**
	 * This method draws different kinds of messages.
	 * 
	 * @param g
	 */
	public abstract void paintMessage(Graphics g);

}
