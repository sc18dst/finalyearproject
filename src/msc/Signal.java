package msc;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Signal extends Message {

	public enum Kind {
		NORMAL, DASHING, FAIL, DFAIL, SELF // different kinds of signals
	};

	private Kind kind;
	private int arrowCoordDif;
	private int titleTextRectPadding;
	private int titleTextWidth;
	private int titleTextHeight;
	private int titleRectWidth;
	private int upperLowerLineLength = 50;
	private int arcSize = 11;

	public Signal(Node nodeOrigin, Node nodeDestination, String name, int position, Kind kind) {
		super(nodeOrigin, nodeDestination, name, position);
		this.kind = kind;
		init();
	}

	protected void init() {
		super.init();
		arrowCoordDif = -5;
		titleTextRectPadding = 4;
		titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		titleTextHeight = fontMetrics.getHeight();
		titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
	}

	@Override
	public int getWidth() {
		int width = 0;
		if (kind == Kind.SELF) {
			width = (upperLowerLineLength - 5 + arcSize) + 10;
		} else if (kind == Kind.FAIL || kind == Kind.DFAIL) {
			width = titleRectWidth + 50;
		} else {
			width = titleRectWidth + 20;
		}
		return Math.max(0, width);
	}

	@Override
	public int getLeftLead() {
		int lead = 0;
		if (kind == Kind.SELF) {
			lead = titleTextWidth
					- (getNodeOrigin().getCenterX(this) - getNodeOrigin().getLeftX()) + 20;
		}
		return Math.max(0, lead);
	}

	@Override
	public int getRightLead() {
		int lead = 0;
		if (kind == Kind.SELF) {
			lead = (upperLowerLineLength - 5 + arcSize) + (getNodeOrigin().getCenterX(null) - getNodeOrigin().getLeftX()) + 10;
		}
		return Math.max(0, lead);
	}

	@Override
	public void paintMessage(Graphics g) {
		g.setFont(font);
		int y = getBaselineY();
		int x = getNodeOrigin().getCenterX(this);
		int distToNode = 0;
		if (getNodeDestination() != null)
			distToNode = getNodeDestination().getCenterX(this) - getNodeOrigin().getCenterX(this);
		// Normal Arrow
		if (distToNode < 0 && kind == Kind.NORMAL) {
			normalArrowLeft(g, x + distToNode, y, -distToNode); // left
		}
		if (distToNode > 0 && kind == Kind.NORMAL) {
			normalArrowRight(g, x, y, distToNode); // right
		}
		// Dashing Arrow
		if (distToNode < 0 && kind == Kind.DASHING) {
			dashingArrowLeft(g, x + distToNode, y, -distToNode); // left
		}
		if (distToNode > 0 && kind == Kind.DASHING) {
			dashingArrowRight(g, x, y, distToNode); // right
		}
		// Failing Arrow
		if (distToNode < 0 && kind == Kind.FAIL) {
			arrowFailLeft(g, x + distToNode, y, -distToNode); // left
		}
		if (distToNode > 0 && kind == Kind.FAIL) {
			arrowFailRight(g, x, y, distToNode); // right
		}
		// Failing Dashing Arrow
		if (distToNode < 0 && kind == Kind.DFAIL) {
			arrowDashingFailLeft(g, x + distToNode, y, -distToNode); // left
		}
		if (distToNode > 0 && kind == Kind.DFAIL) {
			arrowDashingFailRight(g, x, y, distToNode); // right
		}
		// Arrow to Self
		if (kind == Kind.SELF) {
			arrowToSelf(g, x, y); // to self
		}
	}

	public void normalArrowRight(Graphics g, int x, int y, int distToNode) {
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x, y, x + distToNode, y);
		g.drawLine(x + distToNode + arrowCoordDif, y + arrowCoordDif, x + distToNode, y);
		g.drawLine(x + distToNode + arrowCoordDif, y - arrowCoordDif, x + distToNode, y);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.white);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void normalArrowLeft(Graphics g, int x, int y, int distToNode) {
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x, y, x + distToNode, y);
		g.drawLine(x - arrowCoordDif, y + arrowCoordDif, x, y);
		g.drawLine(x - arrowCoordDif, y - arrowCoordDif, x, y);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void dashingArrowLeft(Graphics g, int x, int y, int distToNode) {
		Graphics2D g2d = (Graphics2D) g;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		float[] dash = { 4f, 0f, 2f };
		BasicStroke nLine = new BasicStroke();
		g2d.setStroke(nLine);
		BasicStroke dLine = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash, 2f);
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x - arrowCoordDif, y + arrowCoordDif, x, y);
		g.drawLine(x - arrowCoordDif, y - arrowCoordDif, x, y);
		g2d.setStroke(dLine);
		g2d.drawLine(x, y, x + distToNode, y);
		g2d.setStroke(nLine);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void dashingArrowRight(Graphics g, int x, int y, int distToNode) {
		Graphics2D g2d = (Graphics2D) g;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		float[] dash = { 4f, 0f, 2f };
		BasicStroke nLine = new BasicStroke();
		g2d.setStroke(nLine);
		BasicStroke dLine = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash, 2f);
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x + distToNode + arrowCoordDif, y + arrowCoordDif, x + distToNode, y);
		g.drawLine(x + distToNode + arrowCoordDif, y - arrowCoordDif, x + distToNode, y);
		g2d.setStroke(dLine);
		g2d.drawLine(x, y, x + distToNode, y);
		g2d.setStroke(nLine);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void arrowFailRight(Graphics g, int x, int y, int distToNode) {
		distToNode -= 20;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x, y, x + distToNode, y);
		g.drawLine(x + distToNode + arrowCoordDif, y + arrowCoordDif, x + distToNode + 5, y + 5);
		g.drawLine(x + distToNode + arrowCoordDif, y - arrowCoordDif, x + distToNode + 5, y - 5);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void arrowFailLeft(Graphics g, int x, int y, int distToNode) {
		x += 20;
		distToNode -= 20;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x, y, x + distToNode, y);
		g.drawLine(x - arrowCoordDif, y + arrowCoordDif, x - 5, y + 5);
		g.drawLine(x - arrowCoordDif, y - arrowCoordDif, x - 5, y - 5);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void arrowDashingFailRight(Graphics g, int x, int y, int distToNode) {
		distToNode -= 20;
		Graphics2D g2d = (Graphics2D) g;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		float[] dash = { 4f, 0f, 2f };
		BasicStroke nLine = new BasicStroke();
		g2d.setStroke(nLine);
		BasicStroke dLine = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash, 2f);
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g2d.setStroke(dLine);
		g2d.drawLine(x, y, x + distToNode, y);
		g2d.setStroke(nLine);
		g.drawLine(x + distToNode + arrowCoordDif, y + arrowCoordDif, x + distToNode + 5, y + 5);
		g.drawLine(x + distToNode + arrowCoordDif, y - arrowCoordDif, x + distToNode + 5, y - 5);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void arrowDashingFailLeft(Graphics g, int x, int y, int distToNode) {
		x += 20;
		distToNode -= 20;
		Graphics2D g2d = (Graphics2D) g;
		int titleTextRectPadding = 4;
		int titleTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int titleTextHeight = fontMetrics.getHeight() - (titleTextRectPadding / 2);
		int titleRectWidth = titleTextWidth + 2 * titleTextRectPadding;
		int titleRectHeight = titleTextHeight + 2 * titleTextRectPadding;
		float[] dash = { 4f, 0f, 2f };
		BasicStroke nLine = new BasicStroke();
		g2d.setStroke(nLine);
		BasicStroke dLine = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash, 2f);
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g2d.setStroke(dLine);
		g2d.drawLine(x, y, x + distToNode, y);
		g2d.setStroke(nLine);
		g.drawLine(x - arrowCoordDif, y + arrowCoordDif, x - 5, y + 5);
		g.drawLine(x - arrowCoordDif, y - arrowCoordDif, x - 5, y - 5);
		int titleX = x + (distToNode - titleTextWidth) / 2;
		int titleY = y + arrowCoordDif;
		int RectX = titleX - titleTextRectPadding;
		int RectY = titleY - titleTextHeight;
		g.setColor(Color.WHITE);
		g.fillRect(RectX, RectY, titleRectWidth, titleRectHeight + arrowCoordDif);
		g.setColor(Color.BLACK);
		g.drawString(getName(), titleX, titleY);
	}

	public void arrowToSelf(Graphics g, int x, int y) {
		int activeX = getNodeOrigin().getCenterX(this);
		int centerX = getNodeOrigin().getCenterX(null);
		int arcStartAngle1 = 360;
		int arcStartAngle2 = 270;
		int arcAngle = 90;
		int sideLineX = 6;
		int sideLineY = 20;
		int lowerArcY = 15;
		x = centerX + (activeX - centerX) * -1;
		Color custRed = new Color(168, 0, 54);
		g.setColor(custRed);
		g.drawLine(x, y, x + upperLowerLineLength, y);
		g.drawArc(x + upperLowerLineLength - 5, y, arcSize, arcSize, arcStartAngle1, arcAngle);
		g.drawLine(x + upperLowerLineLength - 5 + arcSize, y + sideLineX, x + upperLowerLineLength - 5 + arcSize, y + sideLineY);
		g.drawArc(x + upperLowerLineLength - 5, y + lowerArcY, arcSize, arcSize, arcStartAngle2, arcAngle);
		g.drawLine(x, y + sideLineX + sideLineY, x + upperLowerLineLength, y + sideLineX + sideLineY);
		g.drawLine(x, y + sideLineX + sideLineY, x + 5, y + sideLineX + sideLineY - 5);
		g.drawLine(x, y + sideLineX + sideLineY, x + 5, y + sideLineX + sideLineY + 5);
		g.setColor(Color.BLACK);
		// draw text
		x = activeX;
		int titleX = x - titleTextWidth + arrowCoordDif;
		int titleY = y + arrowCoordDif + titleTextHeight;
		g.drawString(getName(), titleX, titleY);
	}

	public Kind getKind() {
		return kind;
	}
}
