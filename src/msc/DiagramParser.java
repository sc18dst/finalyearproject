package msc;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import msc.Note.Type;
import msc.Signal.Kind;

public class DiagramParser {

	private static final String TITLE_COMMAND = "title";
	private static final String SEPARATOR = " ";

	public DiagramParser() {
	}


	public static Node getNode(ArrayList<Node> nodes, String nodeName) {
		for (Node n : nodes) {
			if (n.getName().equals(nodeName)) {
				return n;
			}
		}

		return null;
	}

	private Node getNodeAddIfNew(ArrayList<Node> nodes, String nodeName) {
		Node node = getNode(nodes, nodeName);

		if (node == null) {
			node = new Node(nodeName, nodes.size());
			nodes.add(node);
		}

		return node;
	}

	/**
	 * 
	 * @param messages
	 * @param nodeName
	 * @throws Exception
	 *             if the node is not either origin or destination of a signal
	 */
	private void validateActivatable(ArrayList<Message> messages,
			String nodeName) throws Exception {
		for (Message message : messages) {
			if (message instanceof Signal) {
				if (((Signal) message).getNodeOrigin().getName()
						.equals(nodeName))
					return;

				if (((Signal) message).getNodeDestination().getName()
						.equals(nodeName))
					return;
			}
		}

		throw new IllegalArgumentException("Node [" + nodeName
				+ "] cannot be (de)activated as there are no signals...");
	}

	public DiagramModel parse(String input) throws Exception {
		ArrayList<Node> nodes = new ArrayList<Node>();
		ArrayList<Message> messages = new ArrayList<Message>();
		String title = "";

		Pattern signalPatternDashingFail = Pattern.compile("(.+)x-->(.+):(.*)?");
		Pattern signalPatternDashing = Pattern.compile("(.+)-->(.+):(.*)?");
		Pattern signalPatternFail = Pattern.compile("(.+)x->(.+):(.*)?");
		Pattern signalPatternNormal = Pattern.compile("(.+)->(.+):(.*)");
		Pattern notePatternLeft = Pattern.compile("note left of (.+):(.*)?");
		Pattern notePatternRight = Pattern.compile("note right of (.+):(.*)?");
		Pattern notePatternOver = Pattern.compile("note over (.+):(.*)?");
		Pattern activatePattern = Pattern.compile("activate (.*)");
		Pattern deactivatePattern = Pattern.compile("deactivate (.*)");

		Pattern[] signalPatterns = { signalPatternDashingFail,
				signalPatternDashing, signalPatternFail, signalPatternNormal };

		// parse
		BufferedReader reader = new BufferedReader(new StringReader(input));
		String command;
		while ((command = reader.readLine()) != null) {
			// test command for signal
			Kind kind = Kind.NORMAL;
			Type type = Type.OVER1;
			Matcher matcher = null;
			String[] nodeNames = null;
			for (Pattern signalPattern : signalPatterns) {
				matcher = signalPattern.matcher(command);
				if (matcher.matches()) {
					// set kind
					if (signalPattern == signalPatternDashingFail) {
						kind = Kind.DFAIL;
					} else if (signalPattern == signalPatternDashing) {
						kind = Kind.DASHING;
					} else if (signalPattern == signalPatternFail) {
						kind = Kind.FAIL;
					}

					break;
				}
			}

			if (matcher != null && matcher.matches()) {
				// found signal
				String originNodeName = matcher.group(1).trim();
				String destNodeName = matcher.group(2).trim();
				String name = matcher.group(3).trim();

				Node nodeOrigin = getNodeAddIfNew(nodes, originNodeName);

				// check for activate/deactivate sub-command
				Node nodeDestination = null;

				if (destNodeName.startsWith("+")) {
					destNodeName = destNodeName.substring(1);
					nodeDestination = getNodeAddIfNew(nodes, destNodeName);
					nodeDestination.activateAt(messages.size());
				} else if (destNodeName.startsWith("-")) {
					destNodeName = destNodeName.substring(1);
					nodeDestination = getNodeAddIfNew(nodes, destNodeName);
					nodeDestination.deactivateAt(messages.size());
				} else {
					nodeDestination = getNodeAddIfNew(nodes, destNodeName);
				}

				if (originNodeName.equals(destNodeName))
					kind = Kind.SELF;

				// add message
				messages.add(new Signal(nodeOrigin, nodeDestination, name,
						messages.size() + 1, kind));

				// skip next statements in the loop
				continue;
			}

			matcher = notePatternLeft.matcher(command);
			if (matcher.matches() && matcher != null) {
				type = Type.LEFT;
				String nodeName = matcher.group(1).trim();
				String noteName = matcher.group(2).trim();

				Node node = getNodeAddIfNew(nodes, nodeName);

				messages.add(new Note(node, null, noteName,
						messages.size() + 1, type));

				continue;
			}

			matcher = notePatternRight.matcher(command);
			if (matcher.matches() && matcher != null) {
				type = Type.RIGHT;
				String nodeName = matcher.group(1).trim();
				String noteName = matcher.group(2).trim();

				Node node = getNodeAddIfNew(nodes, nodeName);

				messages.add(new Note(node, null, noteName,
						messages.size() + 1, type));

				continue;
			}

			matcher = notePatternOver.matcher(command);



			if (matcher.matches()) {
				if (matcher.group(1).indexOf(',') != -1) {
					nodeNames = matcher.group(1).split(",");
					type = Type.OVERN;
				}
			}

			if (matcher != null && matcher.matches()) {
				Node originNode;
				Node destNode = null;

				if (nodeNames == null) {
					String originNodeName = matcher.group(1).trim();
					originNode = getNodeAddIfNew(nodes, originNodeName);
					destNode = null;
				} else {
					Node node = getNodeAddIfNew(nodes, nodeNames[0].trim());
					originNode = node;
					destNode = node;

					for (int i = 1; i < nodeNames.length; i++) {
						Node n = getNodeAddIfNew(nodes, nodeNames[i].trim());
						if (n.getNodePosition() < originNode.getNodePosition())
							originNode = n;
						else if (n.getNodePosition() > destNode
								.getNodePosition())
							destNode = n;
					}
				}

				String noteName = matcher.group(2).trim();
				messages.add(new Note(originNode, destNode, noteName, messages
						.size() + 1, type));
				continue;
			}

			Node node = null;
			matcher = activatePattern.matcher(command);

			if (matcher.matches()) {
				String nodeName = matcher.group(1).trim();
				validateActivatable(messages, nodeName);
				if (getNode(nodes, nodeName) != null) {
					node = getNode(nodes, nodeName);
					node.activateAt(messages.size() - 1);

					continue;
				}
			}

			matcher = deactivatePattern.matcher(command);
			if (matcher.matches()) {
				String nodeName = matcher.group(1).trim();
				validateActivatable(messages, nodeName);
				if (getNode(nodes, nodeName) != null) {
					node = getNode(nodes, nodeName);
					node.deactivateAt(messages.size());

					continue;
				}
			}


			if (command.startsWith(TITLE_COMMAND + SEPARATOR)) {
				// title command
				title = command.substring((TITLE_COMMAND + SEPARATOR).length())
						.trim();

				continue;
			}
			if (command.trim().equals("")) {
				continue;
			}
			throw new IllegalArgumentException("Unknown command!");
		}

		// make sure we deactivate all active nodes and also set messages#
		for (Node node : nodes) {
			node.deactivateAt(messages.size());
			node.setMessageCount(messages.size());
		}

		// construct diagram model
		DiagramModel model = new DiagramModel(nodes, messages, title);
		return model;
	}

}
