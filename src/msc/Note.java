package msc;

import java.awt.Color;
import java.awt.Graphics;

public class Note extends Message {

	public enum Type {
		LEFT, RIGHT, OVER1, OVERN // different types of notes
	};

	private Type type;
	private int noteTriangleCoordsDif = 5;
	private int labelTextPadding = 10;
	private int labelTextWidth;
	private int labelTextHeight;
	private int labelRectWidth;
	private int labelRectHeight;



	public Note(Node nodeOrigin, Node nodeDestination, String name,
			int position, Type type) {
		super(nodeOrigin, nodeDestination, name, position);
		this.type = type;
		init();
	}

	@Override
	protected void init() {
		super.init();

		labelTextWidth = fontMetrics.charsWidth(getName().toCharArray(), 0,
				getName().length());
		labelTextHeight = fontMetrics.getHeight() - (labelTextPadding / 2);
		labelRectWidth = labelTextWidth + 2 * labelTextPadding;
		labelRectHeight = labelTextHeight + 2 * labelTextPadding;
	}


	@Override
	public int getWidth() {
		if (type == Type.OVERN) {
			return labelRectWidth - 2 * labelTextPadding;
		} else {
			return 0;
		}
	}

	@Override
	public int getLeftLead() {
		if (type == Type.RIGHT || type == Type.OVERN) {
			return 0;
		}

		int lead = 0;
		if (type == Type.LEFT) {
			lead = labelRectWidth
					+ 2
					* labelTextPadding
					- (getNodeOrigin().getCenterX(null) - getNodeOrigin()
							.getLeftX());
		} else if (type == Type.OVER1) {
			lead = (labelRectWidth + labelTextPadding)
					/ 2
					- (getNodeOrigin().getCenterX(null) - getNodeOrigin()
							.getLeftX());
		}

		return Math.max(0, lead);
	}

	@Override
	public int getRightLead() {
		if (type == Type.LEFT || type == Type.OVERN) {
			return 0;
		}

		int lead = 0;
		if (type == Type.RIGHT) {
			lead = labelRectWidth
					+ 3
					* labelTextPadding
					+ (getNodeOrigin().getCenterX(null) - getNodeOrigin()
							.getLeftX());
		} else if (type == Type.OVER1) {
			lead = (labelRectWidth + 2 * labelTextPadding)
					/ 2
					+ (getNodeOrigin().getCenterX(null) - getNodeOrigin()
							.getLeftX());
		}

		return Math.max(0, lead);
	}

	@Override
	public void paintMessage(Graphics g) {
		g.setFont(font);

		int y = getBaselineY();
		int x = getNodeOrigin().getCenterX(null) - labelTextPadding;
		int distToNode = 0;

		if (getNodeDestination() != null) {
			distToNode = getNodeDestination().getCenterX(null)
					- getNodeOrigin().getCenterX(null);

			if (getNodeOrigin().getNodePosition()
					- getNodeDestination().getNodePosition() > 0) {
				x = getNodeDestination().getCenterX(null);

			}

			if (distToNode < 0) {
				distToNode = -distToNode;

			}
		}

		paintNote(g, x, y, distToNode);
	}

	public void paintNote(Graphics g, int x, int y, int distToNode) {
		if ((distToNode >= labelRectWidth - 2 * labelTextPadding)
				&& (type == Type.OVERN)) {
			labelRectWidth = distToNode + 2 * labelTextPadding;
		}

		if ((distToNode < labelRectWidth - 2 * labelTextPadding)
				&& (type == Type.OVERN)) {
			int centPointBetwNodes = (getNodeDestination().getCenterX(null) - getNodeOrigin()
					.getCenterX(null)) / 2;


			if (centPointBetwNodes < 0) {
				centPointBetwNodes = -centPointBetwNodes;
			}

			if (getNodeOrigin().getNodePosition() > getNodeDestination()
					.getNodePosition()) {
				x = getNodeOrigin().getCenterX(null) - centPointBetwNodes
						- labelRectWidth / 2;
			} else {
				x = getNodeDestination().getCenterX(null) - centPointBetwNodes
						- labelRectWidth / 2;
			}
		}

		if (type == Type.LEFT) {
			x = getNodeOrigin().getCenterX(null) - labelRectWidth - 2
					* noteTriangleCoordsDif;
		}
		if (type == Type.RIGHT) {
			x = getNodeOrigin().getCenterX(null) + 2 * noteTriangleCoordsDif;
		}

		if (type == Type.OVER1) {
			x = getNodeOrigin().getCenterX(null) - labelRectWidth / 2;
		}

		int[] xPoints = { x, // 1
				x, // 2
				x + labelRectWidth, // 3
				x + labelRectWidth, // 4
				x + labelRectWidth - noteTriangleCoordsDif, // 5
				x + labelRectWidth - noteTriangleCoordsDif, // 6
				x + labelRectWidth, // 7
				x + labelRectWidth - noteTriangleCoordsDif }; // 8
		int[] yPoints = { y, // 1
				y + labelRectHeight, // 2
				y + labelRectHeight, // 3
				y + noteTriangleCoordsDif, // 4
				y + noteTriangleCoordsDif, // 5
				y, // 6
				y + noteTriangleCoordsDif, // 7
				y }; // 8

		Color custYellow = new Color(255, 255, 207);
		Color custRed = new Color(168, 0, 54);
		g.setColor(custYellow);
		g.fillPolygon(xPoints, yPoints, xPoints.length);
		g.setColor(custRed);
		g.drawPolygon(xPoints, yPoints, xPoints.length);
		g.setColor(Color.BLACK);

		int labelX = x + (labelRectWidth - labelTextWidth) / 2;
		int labelY = y + labelTextPadding + labelTextHeight;
		g.drawString(getName(), labelX, labelY);
	}
}
