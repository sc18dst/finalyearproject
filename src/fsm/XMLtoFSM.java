package fsm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class XMLtoFSM {

	private String filePath = "";
	private ArrayList<State> states = null;
	private ArrayList<Connection> connections = null;
	private String fileInfo = "";


	public XMLtoFSM(String filePath) {
		this.filePath = filePath;
		generateStringFromFile();
		generateFSMFromString(0, fileInfo.length());
	}
	
	public XMLtoFSM(String fileContents, int startIndex, int endIndex) {
		this.fileInfo = fileContents;
		generateFSMFromString(startIndex, endIndex);
	}

	private void generateStringFromFile() {
		try (Scanner fileReader = new Scanner(new File(filePath))) {
			StringBuilder sb = new StringBuilder();
			while (fileReader.hasNextLine()) {
				sb.append(fileReader.nextLine() + "\n");
			}
			fileInfo = sb.toString();
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	private void generateFSMFromString(int startIndex, int endIndex) {
		if (!fileInfo.trim().equals("")) {
			states = new ArrayList<>();
			connections = new ArrayList<>();
			int index = fileInfo.indexOf("<state>", startIndex);
			while (index >= 0 && index < endIndex) {
				if (states.isEmpty()) {
					states.add(getStateInfo(index));
					index = fileInfo.indexOf("<state>", index + 1);
					continue;
				}
				addStateIfNotPresent(getStateInfo(index));
				index = fileInfo.indexOf("<state>", index + 1);
			}
			index = fileInfo.indexOf("<arrow>", startIndex);
			while (index >= 0 && index < endIndex) {
				if (connections.isEmpty()) {
					Arrow arrow = getArrowInfo(index);
					Connection connection = new Connection(arrow.getStateFrom(), arrow.getStateTo());
					connection.getArrows().add(arrow);
					connections.add(connection);
					index = fileInfo.indexOf("<arrow>", index + 1);
					continue;
				}
				addConnectionIfStatesNotPresent(getArrowInfo(index));
				index = fileInfo.indexOf("<arrow>", index + 1);
			}
		}
	}

	private void addConnectionIfStatesNotPresent(Arrow arrow) {
		String nameOfState1 = arrow.getStateFrom().getName();
		String nameOfState2 = arrow.getStateTo().getName();


		for (Connection connection : connections) {
			if (!connection.getState1().getName().equals(connection.getState2().getName()) && !nameOfState1.equals(nameOfState2)) {
				if (connection.getState1().getName().equals(nameOfState1) || connection.getState1().getName().equals(nameOfState2)) {
					if (connection.getState2().getName().equals(nameOfState1) || connection.getState2().getName().equals(nameOfState2)) {
						connection.getArrows().add(arrow);
						return;
					}
				}
			}
			if (connection.getState1().getName().equals(connection.getState2().getName()) && nameOfState1.equals(nameOfState2)) {
				if (connection.getState1().getName().equals(nameOfState1)) {
					connection.getArrows().add(arrow);
					return;
				}
			}
		}
		Connection connection = new Connection(arrow.getStateFrom(), arrow.getStateTo());
		connection.getArrows().add(arrow);
		connections.add(connection);
	}

	private Arrow getArrowInfo(int startIndex) {
		int startIndexOfType = fileInfo.indexOf("<type>", startIndex) + 6;
		int startIndexOfStateFrom = fileInfo.indexOf("<stateFrom>", startIndex) + 11;
		int startIndexOfStateTo = fileInfo.indexOf("<stateTo>", startIndex) + 9;
		int startIndexOfArrowName = fileInfo.indexOf("<name>", startIndex) + 6;
		int startIndexOfArrowAngle = fileInfo.indexOf("<angle>", startIndex) + 7;
		int endIndexOfType = fileInfo.indexOf("</type>", startIndex);
		int endIndexOfStateFrom = fileInfo.indexOf("</stateFrom>", startIndex);
		int endIndexOfStateTo = fileInfo.indexOf("</stateTo>", startIndex);
		int endIndexOfArrowName = fileInfo.indexOf("</name>", startIndex);
		int endIndexOfArrowAngle = fileInfo.indexOf("</angle>", startIndex);
		String arrowType = fileInfo.substring(startIndexOfType, endIndexOfType);
		String stateFromName = fileInfo.substring(startIndexOfStateFrom, endIndexOfStateFrom);
		String stateToName = fileInfo.substring(startIndexOfStateTo, endIndexOfStateTo);
		String arrowName = fileInfo.substring(startIndexOfArrowName, endIndexOfArrowName);
		String arrowAngle = fileInfo.substring(startIndexOfArrowAngle, endIndexOfArrowAngle);
		State stateFrom = getStateByName(stateFromName);
		State stateTo = getStateByName(stateToName);
		Arrow arrow = null;
		if (arrowType.equals("Curved")) {
			arrow = new CurvedArrow(stateFrom, stateTo, arrowName, Integer.parseInt(arrowAngle));
		}
		if (arrowType.equals("Normal")) {
			arrow = new NormalArrow(stateFrom, stateTo, arrowName);
		}
		if (arrowType.equals("Self")) {
			arrow = new SelfArrow(stateFrom, stateTo, arrowName);
			((SelfArrow) arrow).setAngle(Integer.parseInt(arrowAngle));
		}
		return arrow;
	}

	private State getStateByName(String name) {
		for (State state : states) {
			if (name.equals(state.getName())) {
				return state;
			}
		}
		return null;
	}

	private void addStateIfNotPresent(State state) {
		for (State s : states) {
			if (s.getName().equals(state.getName())) {
				return;
			}
		}
		states.add(state);
	}

	private State getStateInfo(int startIndex) {
		int startIndexOfStateX = fileInfo.indexOf("<x>", startIndex) + 3;
		int startIndexOfStateY = fileInfo.indexOf("<y>", startIndex) + 3;
		int startIndexOfStateName = fileInfo.indexOf("<name>", startIndex) + 6;
		int endIndexOfStateX = fileInfo.indexOf("</x>", startIndex);
		int endIndexOfStateY = fileInfo.indexOf("</y>", startIndex);
		int endIndexOfStateName = fileInfo.indexOf("</name>", startIndex);
		int stateX = Integer.parseInt(fileInfo.substring(startIndexOfStateX, endIndexOfStateX));
		int stateY = Integer.parseInt(fileInfo.substring(startIndexOfStateY, endIndexOfStateY));
		String stateName = fileInfo.substring(startIndexOfStateName, endIndexOfStateName);
		State state = new State(stateX, stateY, stateName);
		return state;
	}

	public ArrayList<State> getStates() {
		return states;
	}

	public ArrayList<Connection> getConnections() {
		return connections;
	}
}
