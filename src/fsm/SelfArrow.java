package fsm;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;

public class SelfArrow extends Arrow {

	private int angle = 45;
	private Shape arrArc;
	private Polygon arrowCap;
	private AffineTransform arrowArcTransform;
	private AffineTransform arrowCapTransform;
	private AffineTransform nameTransform;
	private Rectangle2D nameBounds;

	public SelfArrow(State s1, State s2, String name) {
		super(s1, s2, name);
	}

	@Override
	public void paintArrow(Graphics g1) {
		Graphics2D g = (Graphics2D) g1.create();
		BasicStroke stroke = new BasicStroke(2.5f);
		g.setColor(getArrowColor());
		double x1 = getStateFrom().getX() + getStateFrom().getDiameter() / 2;
		double y1 = getStateFrom().getY() + getStateFrom().getDiameter() / 2;
		double d = getStateFrom().getDiameter();
		double arrowD = d / 2 - d / 10;
		double newX = d / 2 - arrowD / 2;
		double newY = -arrowD / 2;
		double capX = (d * d / 2 - arrowD * arrowD / 4) / (2 * d / 2);
		double capY = Math.sqrt(d * d / 4 - capX * capX);
		double angle1 = Math.atan2(capY - (newY + arrowD / 2), capX - (newX + arrowD / 2));
		double angle2 = Math.atan2(-capY - (newY + arrowD / 2), capX - (newX + arrowD / 2));
		double result = angle1 - angle2;
		AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
		at.concatenate(AffineTransform.getRotateInstance(Math.toRadians(angle)));
		g.transform(at);
		g.rotate(Math.toRadians(-angle));
		Font font = g.getFont();
		FontMetrics fontMetrics = getFontMetrics(font);
		int nameWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int nameHeight = fontMetrics.getHeight();
		g.setColor(getTextColor());

		if (angle == 45) {
			g.drawString(getName(), (int) (capX + arrowD / 4), (int) (capY + arrowD));
			nameBounds = new Rectangle2D.Double(capX + arrowD / 4, capY + arrowD - nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
		}
		if (angle == 135) {
			g.drawString(getName(), (int) (-capX - arrowD / 4 - nameWidth), (int) (capY + arrowD));
			nameBounds = new Rectangle2D.Double(-capX - arrowD / 4 - nameWidth, capY + arrowD - nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
		}
		if (angle == 225) {
			g.drawString(getName(), (int) (-newX - d / 4 - nameWidth - 3), (int) (newY - d / 4));
			nameBounds = new Rectangle2D.Double(-newX - d / 4 - nameWidth, newY - d / 4 - nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
		}


		if (angle == 315) {
			g.drawString(getName(), (int) (newX + d / 4 + 3), (int) (newY - d / 4));
			nameBounds = new Rectangle2D.Double(newX + d / 4, newY - d / 4 - nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
		}
		g.setColor(getArrowColor());
		g.rotate(Math.toRadians(angle));
		arrowArcTransform = (AffineTransform) at.clone();
		arrowCapTransform = (AffineTransform) at.clone();
		Arc2D arrowArc = new Arc2D.Double(newX, newY, arrowD, arrowD, 360 - Math.toDegrees(result) / 2, Math.toDegrees(result), Arc2D.OPEN);
		g.draw(arrowArc);
		arrArc = stroke.createStrokedShape(arrowArc);
		AffineTransform at2 = AffineTransform.getTranslateInstance(capX, capY);
		at2.concatenate(AffineTransform.getRotateInstance(Math.toRadians(10)));
		g.transform(at2);
		arrowCapTransform.concatenate(at2);
		arrowCap = new Polygon(new int[] { 0, getArrSize(), getArrSize(), 0 }, new int[] { 0, -getArrSize(), getArrSize(), 0 }, 4);
		g.fillPolygon(arrowCap);
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}



	public Shape getArrowArc() {
		return arrArc;
	}

	public Polygon getArrowCap() {
		return arrowCap;
	}

	public AffineTransform getArrowArcTransform() {
		return arrowArcTransform;
	}

	public AffineTransform getArrowCapTransform() {
		return arrowCapTransform;
	}

	public AffineTransform getNameTransform() {
		return nameTransform;
	}

	public Rectangle2D getNameBounds() {
		return nameBounds;
	}
}
