package fsm;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;

public class NormalArrow extends Arrow {

	private Path2D arr;
	private Path2D arrowCap;
	private AffineTransform nameTransform;
	private Rectangle2D nameBounds;

	public NormalArrow(State s1, State s2, String name) {
		super(s1, s2, name);
	}

	@Override
	public void paintArrow(Graphics g1) {
		Graphics2D g = (Graphics2D) g1.create();
		g.setColor(getArrowColor());
		BasicStroke stroke = new BasicStroke(2.5f);
		int x1 = getStateFrom().getX() + getStateFrom().getDiameter() / 2;
		int x2 = getStateTo().getX() + getStateTo().getDiameter() / 2;
		int y1 = getStateFrom().getY() + getStateFrom().getDiameter() / 2;
		int y2 = getStateTo().getY() + getStateTo().getDiameter() / 2;
		int d1 = getStateFrom().getDiameter();
		int d2 = getStateTo().getDiameter();
		double dx = x2 - x1, dy = y2 - y1;
		double angle = Math.atan2(dy, dx);
		int len = (int) Math.sqrt(dx * dx + dy * dy);
		AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
		at.concatenate(AffineTransform.getRotateInstance(angle));
		g.transform(at);
		int newLen = len - d2 / 2;
		Line2D arrow = new Line2D.Double(0 + d1 / 2, 0, newLen, 0);
		g.draw(arrow);
		Font font = g.getFont();
		FontMetrics fontMetrics = getFontMetrics(font);
		int namePadding = 6;
		int nameWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int nameHeight = fontMetrics.getHeight();
		g.setColor(getTextColor());
		if (angle > -1.571 && angle < 1.57) {
			g.drawString(getName(), -nameWidth / 2 + len / 2, -nameHeight / 2 + namePadding);
			nameBounds = new Rectangle2D.Double(-nameWidth / 2 + len / 2, -nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
		} else {
			g.rotate(Math.toRadians(180));
			g.drawString(getName(), -len / 2 - nameWidth / 2, -nameHeight / 2 + namePadding);
			nameBounds = new Rectangle2D.Double(-len / 2 - nameWidth / 2, -nameHeight, nameWidth, nameHeight); // <--
			nameTransform = (AffineTransform) g.getTransform().clone();
			g.rotate(Math.toRadians(-180));
		}
		g.setColor(getArrowColor());
		PathIterator it = arrow.getPathIterator(at);
		double[] lineX = new double[2];
		double[] lineY = new double[2];
		double[] modXY = new double[2];
		int index = 0;
		while (!it.isDone()) {
			it.currentSegment(modXY);
			lineX[index] = modXY[0];
			lineY[index] = modXY[1];
			if (index != 1) {
				index++;
			}
			it.next();
		}
		arr = new Path2D.Double();
		arr.moveTo(lineX[0], lineY[0]);
		arr.lineTo(lineX[1], lineY[1]);
		arr.closePath();
		arr = (Path2D) stroke.createStrokedShape(arr);
		double[] capX = { newLen, newLen - getArrSize(), newLen - getArrSize(), newLen };
		double[] capY = { 0, -getArrSize(), getArrSize(), 0 };
		Path2D path = new Path2D.Double();
		path.moveTo(capX[0], capY[0]);
		for (int i = 1; i < capX.length; i++) {
			path.lineTo(capX[i], capY[i]);
		}
		path.closePath();
		g.fill(path);
		it = path.getPathIterator(at);
		index = 0;
		while (!it.isDone()) {
			it.currentSegment(modXY);
			capX[index] = modXY[0];
			capY[index] = modXY[1];
			it.next();
			if (index != 3) {
				index++;
			}
		}
		arrowCap = new Path2D.Double();
		arrowCap.moveTo(capX[0], capY[0]);
		for (int i = 1; i < capX.length; i++) {
			arrowCap.lineTo(capX[i], capY[i]);
		}
		arrowCap.closePath();
	}

	public Path2D getArrow() {
		return arr;
	}

	public Path2D getArrowCap() {
		return arrowCap;
	}

	public AffineTransform getNameTransform() {
		return nameTransform;
	}

	public Rectangle2D getNameBounds() {
		return nameBounds;
	}
}
