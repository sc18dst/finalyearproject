package fsm;

import java.util.ArrayList;

public class Connection {

	private State state1;
	private State state2;
	private ArrayList<Arrow> arrows;

	public Connection(State s1, State s2) {
		this.state1 = s1;
		this.state2 = s2;
		arrows = new ArrayList<>();
	}

	public State getState1() {
		return state1;
	}

	public State getState2() {
		return state2;
	}


	public ArrayList<Arrow> getArrows() {
		return arrows;
	}
}
