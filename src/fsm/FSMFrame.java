package fsm;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import msc.Diagram;

public class FSMFrame extends JInternalFrame {

	private JMenuItem menuItemNew = new JMenuItem("New");
	private JMenuItem menuItemOpen = new JMenuItem("Open FSM..");
	private JMenuItem menuItemSave = new JMenuItem("Save FSM..");
	private JMenuItem menuItemClose = new JMenuItem("Close");
	private JButton editButton;
	private JButton stateButton;
	private JToggleButton connectButton;
	private FSMDiagram fsmDiagram;
	private boolean textPressed = false;
	private boolean imagePressed = false;
	private Diagram diag;
	private JScrollPane diagPane;
	private JScrollPane fsmDiagPane;
	private JMenuBar menuBar = new JMenuBar();
	private JMenu fileMenu = new JMenu("File");
	private JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);

	public FSMFrame(String title, Diagram diag) {
		super(title, true, true, true, true);
		this.diag = diag;
		diagPane = new JScrollPane(diag, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		fsmDiagram = new FSMDiagram(this);
		setFrameLayout();
		setButtonsForToolbar();
		addActionListenersToButtons();
		setLocation(10, 10);
		setPreferredSize(new Dimension(800, 700));
		setVisible(true);
	}

	private void setButtonsForToolbar() {
		ImageIcon stateIcon = new ImageIcon(getClass().getResource("images/stateIcon.png"));
		ImageIcon connectIcon = new ImageIcon(getClass().getResource("images/connectIcon.png"));
		ImageIcon editIcon = new ImageIcon(getClass().getResource("images/editIcon.png"));
		stateButton = new JButton(stateIcon);
		connectButton = new JToggleButton(connectIcon);
		editButton = new JButton(editIcon);
		connectButton.setFocusable(false);
		stateButton.setFocusable(false);
		editButton.setFocusable(false);
		stateButton.setToolTipText("add state");
		connectButton.setToolTipText("add connection");
		editButton.setToolTipText("rename element");
		toolBar.add(stateButton);
		toolBar.add(connectButton);
		toolBar.add(new JSeparator());
		toolBar.add(editButton);
		toolBar.setFloatable(false);
	}

	private void setFrameLayout() {
		ImageIcon iconNew = new ImageIcon(getClass().getResource("images/menuNew.png"));
		ImageIcon iconOpen = new ImageIcon(getClass().getResource("images/menuOpen.png"));
		ImageIcon iconSave = new ImageIcon(getClass().getResource("images/menuSave.png"));
		ImageIcon iconExit = new ImageIcon(getClass().getResource("images/menuExit.png"));
		fileMenu.add(menuItemNew);
		fileMenu.add(menuItemOpen);
		fileMenu.add(menuItemSave);
		fileMenu.add(menuItemClose);
		menuItemNew.setIcon(iconNew);
		menuItemOpen.setIcon(iconOpen);
		menuItemSave.setIcon(iconSave);
		menuItemClose.setIcon(iconExit);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuItemNew.setMnemonic(KeyEvent.VK_N);
		menuItemNew.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemOpen.setMnemonic(KeyEvent.VK_O);
		menuItemOpen.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemSave.setMnemonic(KeyEvent.VK_S);
		menuItemSave.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		menuItemClose.setMnemonic(KeyEvent.VK_X);
		menuItemClose.setAccelerator(KeyStroke.getKeyStroke('X', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		JSeparator separator = new JSeparator();
		separator.setVisible(false);
		menuItemClose.add(separator);
		menuBar.add(fileMenu);
		GridBagLayout grid = new GridBagLayout();
		setLayout(grid);
		diagPane.setPreferredSize(new Dimension(200, 200));
		fsmDiagPane = new JScrollPane(fsmDiagram, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		fsmDiagPane.setPreferredSize(new Dimension(200, 200));
		setConstraintsForComponent(GridBagConstraints.NORTHWEST, 1, 0, 7, 6, 1, 1, fsmDiagPane);
		setConstraintsForComponent(GridBagConstraints.NORTH, 8, 0, 6, 5, 0.05, 1, diagPane);
		setConstraintsForComponent(GridBagConstraints.SOUTH, 0, 0, 1, 1, 0, 0, toolBar);
		setJMenuBar(menuBar);
	}

	private void setConstraintsForComponent(int anchor, int x, int y, int width, int height, double weightx, double weighty, JComponent comp) {
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = anchor;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = x;
		c.gridy = y;
		c.gridwidth = width;
		c.gridheight = height;
		c.weightx = weightx;
		c.weighty = weighty;
		add(comp, c);
	}

	private void addActionListenersToButtons() {
		stateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				fsmDiagram.addState();
			}
		});
		connectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				fsmDiagram.isConnectionClicked();
			}
		});
		menuItemNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the FSM diagram? Any unsaved changes will be lost!", "New FSM Diagram", JOptionPane.YES_NO_OPTION);
				if (optionChosen == JOptionPane.YES_OPTION) {
					fsmDiagram.clearDiagram();
				}
			}
		});
		editButton.setMnemonic(KeyEvent.VK_R);
		editButton.addActionListener(new ActionListener() {



			@Override
			public void actionPerformed(ActionEvent e) {
				fsmDiagram.changeElementName();
			}
		});
		menuItemOpen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileFilter() {

					@Override
					public String getDescription() {
						return "XML File (*.xml)";
					}

					@Override
					public boolean accept(File file) {
						return (file.getName().toLowerCase().endsWith(".xml") || file.isDirectory());
					}
				};
				fileChooser.setFileFilter(filter);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int val = fileChooser.showOpenDialog(null);
				if (val == JFileChooser.APPROVE_OPTION) {
					int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to close the existing FSM diagram? Any unsaved changes will be lost!", "Open FSM Diagram", JOptionPane.YES_NO_OPTION);
					if (optionChosen == JOptionPane.YES_OPTION) {
						File file = fileChooser.getSelectedFile();
						XMLtoFSM fsmDiag = new XMLtoFSM(file.getAbsolutePath());
						fsmDiagram.setStates(fsmDiag.getStates());
						fsmDiagram.setConnections(fsmDiag.getConnections());
						fsmDiagram.repaint();
					}
				}
			}
		});
		menuItemSave.addActionListener(new SaveFsmListener());
		menuItemClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int optionChosen = JOptionPane.showConfirmDialog(null, "Are you sure you want to close the FSM diagram? Any unsaved changes will be lost!", "Close", JOptionPane.YES_NO_OPTION);
				if (optionChosen == JOptionPane.YES_OPTION) {
					dispose();
				}
			}
		});
	}

	class SaveFsmListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			final JDialog dialog = new JDialog();
			ImageIcon frameIcon = new ImageIcon(getClass().getResource("images/menuSave.png"));
			dialog.setIconImage(frameIcon.getImage());
			GridBagLayout layout = new GridBagLayout();
			GridBagConstraints c = new GridBagConstraints();
			final JButton ok = new JButton("OK");
			ok.setEnabled(false);
			ok.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (textPressed == true) {
						JFileChooser fileChooser = new JFileChooser();
						FileFilter filter = new FileFilter() {

							@Override
							public String getDescription() {
								return "XML File (*.xml)";
							}

							@Override
							public boolean accept(File file) {
								return (file.getName().toLowerCase().endsWith(".xml") || file.isDirectory());
							}
						};
						fileChooser.setFileFilter(filter);
						fileChooser.setAcceptAllFileFilterUsed(false);
						int val = fileChooser.showSaveDialog(null);
						
if (val == JFileChooser.APPROVE_OPTION) {
							try {
								File file = new File(fileChooser.getSelectedFile().getPath());
								if (!fileChooser.getSelectedFile().getName().endsWith(".xml")) {
									file = new File(fileChooser.getSelectedFile() + ".xml");
								}
								if (file.exists()) {
									val = JOptionPane.showConfirmDialog(fileChooser, "Replace existing file?");
									if (val == JOptionPane.CLOSED_OPTION) {
										return;
									} else if (val == JOptionPane.YES_OPTION) {
										new FSMtoXML(fsmDiagram.getStates(), fsmDiagram.getConnections(), getTitle(), file.getAbsolutePath());
										JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
										dialog.dispose();
									}
								} else {
									new FSMtoXML(fsmDiagram.getStates(), fsmDiagram.getConnections(), getTitle(), file.getAbsolutePath());
									JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
									dialog.dispose();
								}
							} catch (Exception s) {
								System.err.println(s.getMessage());
							}
						}
					} else if (imagePressed == true) {
						JFileChooser fileChooser = new JFileChooser();
						FileFilter filter = new FileFilter() {

							@Override
							public String getDescription() {
								return "PNG File (*.png)";
							}

							@Override
							public boolean accept(File file) {
								return (file.getName().toLowerCase().endsWith(".png") || file.isDirectory());
							}
						};
						fileChooser.setFileFilter(filter);
						fileChooser.setAcceptAllFileFilterUsed(false);
						int val = fileChooser.showSaveDialog(null);
						if (val == JFileChooser.APPROVE_OPTION) {
							try {
								File file = new File(fileChooser.getSelectedFile().getPath());
								if (!fileChooser.getSelectedFile().getName().endsWith(".png")) {
									file = new File(fileChooser.getSelectedFile() + ".png");
								}
								if (file.exists()) {
									val = JOptionPane.showConfirmDialog(fileChooser, "Replace existing file?");
									if (val == JOptionPane.CLOSED_OPTION) {
										return;
									} else if (val == JOptionPane.YES_OPTION) {
										BufferedImage bi = new BufferedImage(fsmDiagram.getSize().width, fsmDiagram.getSize().height, BufferedImage.TYPE_INT_RGB);
										Graphics g = bi.createGraphics();
										g.setColor(Color.WHITE);
										fsmDiagram.paint(g);
										ImageIO.write(bi, "png", file);
										JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
										dialog.dispose();
									}
								} else {
									BufferedImage bi = new BufferedImage(fsmDiagram.getSize().width, fsmDiagram.getSize().height, BufferedImage.TYPE_INT_RGB);
									Graphics g = bi.createGraphics();
									g.setColor(Color.WHITE);
									fsmDiagram.paint(g);
									ImageIO.write(bi, "png", file);
									JOptionPane.showMessageDialog(null, "File has been successfully saved!", "Save", JOptionPane.INFORMATION_MESSAGE);
									dialog.dispose();
								}
							} catch (Exception s) {
								System.err.println("Error: " + s.getMessage());
							}
						}
					}
				}
			});
			JButton cancel = new JButton("Cancel");


			cancel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() instanceof JButton) {
						Container c = ((JButton) (e.getSource())).getParent();
						while ((c.getParent() != null) && (!(c instanceof JDialog))) {
							c = c.getParent();
						}
						if (c instanceof JDialog) {
							JDialog d = (JDialog) c;
							d.dispose();
						}
					}
				}
			});
			JPanel panel = new JPanel(layout);
			panel.setOpaque(true);
			panel.setBackground(Color.WHITE);
			ButtonGroup bg = new ButtonGroup();
			dialog.setTitle("Save FSM..");
			JRadioButton text = new JRadioButton("Source Text");


			text.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ok.setEnabled(true);
					textPressed = true;
					imagePressed = false;
				}
			});
			final JRadioButton image = new JRadioButton("PNG Image");
			image.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ok.setEnabled(true);
					imagePressed = true;
					textPressed = false;
				}
			});
			text.setBackground(Color.white);
			image.setBackground(Color.white);
			bg.add(text);
			bg.add(image);
			c.fill = GridBagConstraints.CENTER;
			c.insets = new Insets(40, 0, 0, 0);
			c.gridx = 0;
			c.gridy = 0;
			panel.add(text, c);
			c.insets = new Insets(40, 0, 0, 0);
			c.gridx = 1;
			c.gridy = 0;
			panel.add(image, c);
			c.ipadx = 40;
			c.weighty = 1;
			c.anchor = GridBagConstraints.SOUTH;
			c.insets = new Insets(0, 20, 20, 20);
			c.gridx = 0;
			c.gridy = 4;
			panel.add(ok, c);
			c.ipadx = 20;
			c.gridx = 1;
			c.gridy = 4;
			panel.add(cancel, c);
			dialog.add(panel);
			dialog.setResizable(false);
			dialog.setSize(400, 200);
			dialog.setLocationRelativeTo(null);
			dialog.setModal(true);
			dialog.setVisible(true);
		}
	}

	public Diagram getDiag() {
		return this.diag;
	}
	
	public FSMDiagram getFSMDiagram() {
		return this.fsmDiagram;
	}
	public void refreshFSMDiagScrollPane() {
		fsmDiagPane.getViewport().revalidate();
	}
}
