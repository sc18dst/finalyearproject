package fsm;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.Rectangle2D;

public class CurvedArrow extends Arrow {

	private int curveAngle;
	private Shape curvedArrow;
	private Path2D arrowCap;
	private AffineTransform curveTransform;
	private AffineTransform nameTransform;
	private Rectangle2D nameBounds;

	public CurvedArrow(State s1, State s2, String name, int angle) {
		super(s1, s2, name);
		curveAngle = angle;
	}

	@Override
	public void paintArrow(Graphics g1) {
		Graphics2D g = (Graphics2D) g1.create();
		BasicStroke stroke = new BasicStroke(2.5f);
		g.setColor(getArrowColor());
		int x1 = getStateFrom().getX() + getStateFrom().getDiameter() / 2;
		int x2 = getStateTo().getX() + getStateTo().getDiameter() / 2;
		int y1 = getStateFrom().getY() + getStateFrom().getDiameter() / 2;
		int y2 = getStateTo().getY() + getStateTo().getDiameter() / 2;
		int d1 = getStateFrom().getDiameter();
		int d2 = getStateTo().getDiameter();
		double dx = x2 - x1, dy = y2 - y1;
		double angle = Math.atan2(dy, dx);
		int len = (int) Math.sqrt(dx * dx + dy * dy);
		double curvedAngleToRadians = Math.toRadians(curveAngle);
		AffineTransform at = AffineTransform.getTranslateInstance(x1, y1);
		at.concatenate(AffineTransform.getRotateInstance(angle));
		g.transform(at);
		curveTransform = (AffineTransform) at.clone();
		int controlY = 50;
		if (curveAngle < 0) {
			controlY = -controlY;
		}
		double curveX1 = Math.cos(curvedAngleToRadians) * d1 / 2;
		double curveY1 = Math.sin(curvedAngleToRadians) * d1 / 2;
		double curveX2 = len - Math.cos(-curvedAngleToRadians) * d2 / 2;
		double curveY2 = Math.sin(curvedAngleToRadians) * d2 / 2;
		double addAngle1 = Math.atan2(y1 + controlY - y1, x1 + len / 2 - x1);
		double addAngle2 = Math.atan2(y1 - y1, x1 + len / 2 - x1);
		double angleRes = addAngle1 - addAngle2;
		QuadCurve2D curve = new QuadCurve2D.Double(curveX1, curveY1, len / 2, controlY, curveX2, curveY2);
		g.draw(curve);
		curvedArrow = stroke.createStrokedShape(curve);
		Font font = g.getFont();
		FontMetrics fontMetrics = getFontMetrics(font);
		int nameWidth = fontMetrics.charsWidth(getName().toCharArray(), 0, getName().length());
		int nameHeight = fontMetrics.getHeight();
		double curveAngleToDegrees = Math.toDegrees(angle);
		g.setColor(getTextColor());
		if (curveAngleToDegrees >= -90 && Math.toDegrees(angle) < 90) {
			if ((x2 - x1 > 0) || (x2 - x1 == 0 && y2 < y1)) {
				if (curveAngle == 10) {
					g.drawString(getName(), len / 2 - nameWidth / 2, controlY);
					nameBounds = new Rectangle2D.Double(len / 2 - nameWidth / 2, controlY - nameHeight, nameWidth, nameHeight); // <--
					nameTransform = (AffineTransform) g.getTransform().clone();
				}
				if (curveAngle == -10) {
					g.drawString(getName(), len / 2 - nameWidth / 2, controlY + nameHeight / 2);
					nameBounds = new Rectangle2D.Double(len / 2 - nameWidth / 2, controlY + nameHeight / 2 - nameHeight, nameWidth, nameHeight); // <--
					nameTransform = (AffineTransform) g.getTransform().clone();
				}
			}
		} else {
			if ((x2 - x1 < 0) || (x2 - x1 == 0 && y2 > y1)) {
				if (curveAngle == 10) {
					g.rotate(Math.toRadians(180));
					g.drawString(getName(), -len / 2 - nameWidth / 2, -controlY + nameHeight / 2);
					nameBounds = new Rectangle2D.Double(-len / 2 - nameWidth / 2, -controlY + nameHeight / 2 - nameHeight, nameWidth, nameHeight); // <--
					nameTransform = (AffineTransform) g.getTransform().clone();
					g.rotate(Math.toRadians(-180));
				}
				if (curveAngle == -10) {
					g.rotate(Math.toRadians(180));
					g.drawString(getName(), -len / 2 - nameWidth / 2, -controlY);
					nameBounds = new Rectangle2D.Double(-len / 2 - nameWidth / 2, -controlY - nameHeight, nameWidth, nameHeight); // <--
					nameTransform = (AffineTransform) g.getTransform().clone();
					g.rotate(Math.toRadians(-180));
				}
			}
		}
		g.setColor(getArrowColor());
		AffineTransform at2 = AffineTransform.getTranslateInstance(curveX2, curveY2);
		at2.concatenate(AffineTransform.getRotateInstance(-angleRes + Math.toRadians(180)));
		g.transform(at2);
		double[] capX = { 0, getArrSize(), getArrSize(), 0 };
		double[] capY = { 0, getArrSize(), -getArrSize(), 0 };
		Path2D path = new Path2D.Double();
		path.moveTo(capX[0], capY[0]);
		for (int i = 1; i < capX.length; i++) {
			path.lineTo(capX[i], capY[i]);
		}
		path.closePath();
		g.fill(path);
		double[] modXY = new double[2];
		PathIterator it = path.getPathIterator(g.getTransform());
		int index = 0;
		while (!it.isDone()) {
			it.currentSegment(modXY);
			capX[index] = modXY[0];
			capY[index] = modXY[1];
			it.next();
			if (index != 3) {
				index++;
			}
		}
		arrowCap = new Path2D.Double();
		arrowCap.moveTo(capX[0], capY[0]);
		for (int i = 1; i < capX.length; i++) {
			arrowCap.lineTo(capX[i], capY[i]);
		}
		arrowCap.closePath();
	}

	public Shape getCurvedArrow() {
		return curvedArrow;
	}

	public Path2D getArrowCap() {
		return arrowCap;
	}

	public int getCurveAngle() {
		return curveAngle;
	}

	public AffineTransform getCurveTransform() {
		return curveTransform;
	}



	public AffineTransform getNameTransform() {
		return nameTransform;
	}

	public Rectangle2D getNameBounds() {
		return nameBounds;
	}
}
