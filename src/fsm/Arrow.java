package fsm;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

public abstract class Arrow extends JComponent {

	private final static int ARR_SIZE = 6;
	private String name;
	private State stateFrom;
	private State stateTo;
	private Color arrowColor;
	private Color textColor;

	public Arrow(State from, State to, String name) {
		this.stateFrom = from;
		this.stateTo = to;
		this.name = name;
		this.arrowColor = new Color(168, 0, 54);
		this.textColor = Color.BLACK;
	}

	public State getStateFrom() {
		return stateFrom;
	}

	public State getStateTo() {
		return stateTo;
	}

	public void setDraggingColor() {
		this.arrowColor = Color.GREEN;
		this.textColor = Color.GREEN;
	}

	public void setDefaultColor() {
		this.arrowColor = new Color(168, 0, 54);
		this.textColor = Color.BLACK;
	}

	public abstract void paintArrow(Graphics g);

	public static int getArrSize() {
		return ARR_SIZE;
	}

	public Color getArrowColor() {
		return arrowColor;
	}

	public Color getTextColor() {
		return textColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
