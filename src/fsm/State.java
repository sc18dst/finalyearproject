package fsm;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JComponent;

public class State extends JComponent {

	private int x;
	private int y;
	private int diameter;
	private String name;
	private Color colorFill;
	private Color colorBorder;
	private Color colorText;
	Font font;
	FontMetrics fontMetrics;

	public State(int x, int y, String name) {
		this.x = x;
		this.y = y;
		this.diameter = 100;
		this.name = name;
		colorFill = new Color(255, 255, 207);
		colorBorder = new Color(168, 0, 54);
		colorText = Color.BLACK;
		font = new Font(null, Font.BOLD, 17);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int d) {
		this.diameter = d;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void paintState(Graphics g) {
		g.setFont(font);
		g.setColor(colorFill);
		g.fillOval(x, y, diameter, diameter);
		g.setColor(colorBorder);
		g.drawOval(x, y, diameter, diameter);
		g.setColor(colorText);
		fontMetrics = getFontMetrics(font);
		int namePadding = 6;
		int nameWidth = fontMetrics.charsWidth(name.toCharArray(), 0, name.length());
		int nameHeight = fontMetrics.getHeight();
		int nameX = this.x + this.diameter / 2 - nameWidth / 2;
		int nameY = this.y + this.diameter / 2 + nameHeight / 2 - namePadding / 2;
		g.drawString(name, nameX, nameY);
	}

	public void setDraggingColor() {
		colorFill = Color.WHITE;
		colorBorder = Color.GREEN;
		colorText = Color.GREEN;
	}

	public void setDefaultColor() {
		colorFill = new Color(255, 255, 207);
		colorBorder = new Color(168, 0, 54);
		colorText = Color.BLACK;
	}
}
