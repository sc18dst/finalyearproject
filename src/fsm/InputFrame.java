package fsm;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class InputFrame extends JDialog {

	private JTextField area = new JTextField(15);
	private JButton ok = new JButton("Ok");
	private JButton cancel = new JButton("Cancel");
	private JLabel textLabel = new JLabel("Name: ");
	private String input = "";

	public InputFrame(String name, String title) {
		area.setFont(new Font(null, Font.PLAIN, 12));
		if (!name.equals("")) {
			area.setText(name);
			area.selectAll();
		}
		setFrameLayout();
		area.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		textLabel.setFont(new Font(null, Font.PLAIN, 12));
		addActionListeners();
		setSize(190, 110);
		setResizable(false);
		setTitle(title);
		setLocationRelativeTo(null);
		setModal(true);
		getRootPane().setDefaultButton(ok);
		ok.requestFocus();
		setVisible(true);
	}

	private void setFrameLayout() {
		GridBagLayout grid = new GridBagLayout();
		setLayout(grid);
		setConstraintsForComponent(0, 0, 1, 1, 0, new Insets(10, 10, 0, 10), textLabel);
		setConstraintsForComponent(0, 1, 5, 5, 0, new Insets(3, 10, 10, 10), area);
		setConstraintsForComponent(0, 6, 1, 1, 22, new Insets(0, 10, 10, 0), ok);
		setConstraintsForComponent(1, 6, 1, 1, 0, new Insets(0, 25, 10, 10), cancel);
	}

	private void setConstraintsForComponent(int x, int y, int width, int height, int ipadx, Insets insets, JComponent comp) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		c.gridx = x;
		c.gridy = y;
		c.ipadx = ipadx;
		c.gridwidth = width;
		c.gridheight = height;
		c.insets = insets;
		add(comp, c);
	}

	private void addActionListeners() {
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		ok.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] lines = area.getText().split("\n");
				StringBuilder sb = new StringBuilder();
				for (int index = 0; index < lines.length; index++) {
					if (!lines[index].equals("")) {
						sb.append(lines[index]);
					}
					if (index != lines.length - 1) {
						sb.append("\n");
					}
				}
				input = sb.toString();
				if (input.trim().equals("")) {
					JOptionPane.showMessageDialog(null, "Invalid name", "Error", JOptionPane.INFORMATION_MESSAGE);
				} else {
					dispose();
				}
			}
		});
	}

	public String getInput() {
		return this.input;
	}
}
