package fsm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class FSMtoXML {
	private String filePath = "";
	private ArrayList<State> states = null;
	private ArrayList<Connection> connections = null;
	private String result = "";
	private String fsmTitle = "";
	
	public FSMtoXML (ArrayList<State> states, ArrayList<Connection> connections, String fsmTitle, String filePath) {
		this.states = states;
		this.connections = connections;
		this.filePath = filePath;
		this.fsmTitle = fsmTitle;
		convertToXML();

		if (!filePath.equals("")) {
			writeToFile();
		}
	}
	
	private void convertToXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version='1.0' encoding='UTF-8'?>\r\n");
		sb.append("\t<fsmDiagram name = " + fsmTitle + ">\r\n");
		sb.append("\t\t<states>\r\n");
		sb.append(result);
		for (State state : states) {
			sb.append(generateStateBody(state));
		}
		sb.append("\t\t</states>\r\n");
		sb.append("\t\t<arrows>\r\n");
		for (Connection connection : connections) {
			for (Arrow arrow : connection.getArrows()) {
				sb.append(generateArrowBody(arrow));
			}
		}
		sb.append("\t\t</arrows>\r\n");
		sb.append("\t</fsmDiagram name = " + fsmTitle + ">\r\n");
		result = sb.toString();
	}
	
	private String generateArrowBody(Arrow arrow) {
		String arrowType = getArrowType(arrow);
		String arrowAngle = getArrowAngle(arrow);
		String arrowBody = "\t\t\t<arrow>\r\n" + 
								"\t\t\t\t<type>" + arrowType + "</type>\r\n" +
								"\t\t\t\t<stateFrom>" + arrow.getStateFrom().getName() + "</stateFrom>\r\n" + 
								"\t\t\t\t<stateTo>" + arrow.getStateTo().getName() + "</stateTo>\r\n" +
								"\t\t\t\t<name>" + arrow.getName() + "</name>\r\n" + 
								"\t\t\t\t<angle>" + arrowAngle + "</angle>\r\n" +
						   "\t\t\t</arrow>\r\n";
		return arrowBody;
	}
	
	private String getArrowType(Arrow arrow) {
		String arrowType = "";
		if (arrow instanceof CurvedArrow) {
			arrowType = "Curved";
		}
		if (arrow instanceof NormalArrow) {
			arrowType = "Normal";
		}
		if (arrow instanceof SelfArrow) {
			arrowType = "Self";
		}
		return arrowType;
	}
	

	private String getArrowAngle(Arrow arrow) {
		String arrowAngle = "NA";
		if (arrow instanceof CurvedArrow) {
			arrowAngle = Integer.toString(((CurvedArrow) arrow).getCurveAngle());
		}
		if (arrow instanceof SelfArrow) {
			arrowAngle = Integer.toString(((SelfArrow) arrow).getAngle());
		}
		return arrowAngle;
	}
	
	private String generateStateBody(State state) {
		String stateBody = "\t\t\t<state>\r\n" + 
								"\t\t\t\t<x>" + state.getX() + "</x>\r\n" +
								"\t\t\t\t<y>" + state.getY() + "</y>\r\n" + 
								"\t\t\t\t<name>" + state.getName() + "</name>\r\n" +
						   "\t\t\t</state>\r\n";
		return stateBody;
	}

	private void writeToFile() {
		File statesFile = new File(filePath);
		try (FileWriter fileWriter = new FileWriter(statesFile); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
			if (!states.isEmpty() || !(states == null)) {
				if (!result.equals("")) {
					bufferedWriter.write(result);
				}
			}
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	public String getResult() {
		return result;
	}
}
