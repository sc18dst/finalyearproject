package fsm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class FSMDiagram extends JComponent {

	private final static int SNAP_GRID_INTERVAL = 10;
	private FSMFrame frame;
	private ArrayList<State> states = new ArrayList<State>();
	private ArrayList<Connection> connections = new ArrayList<>();
	private int stateDelIndex = -1;
	private int connectionDelIndex = -1;
	private int arrowDelIndex = -1;
	private boolean connectionClicked = false;
	private boolean foundSelectedElement = false;
	private Point lastPoint;
	private State selectedState = null;
	private State previousSelectedState = null;
	private Arrow selectedArrow = null;
	private Arrow previousSelectedArrow = null;



	public FSMDiagram(FSMFrame frame) {
		this.frame = frame;
		MouseListener mouseListener = new MouseListener();
		addMouseMotionListener(mouseListener);
		addMouseListener(mouseListener);
		addMouseWheelListener(new ScaleHandler());
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new KeyDispatcher());
		setPreferredSize(new Dimension(1600, 1000));
	}

	@Override
	public void paintComponent(Graphics g1) {
		Graphics2D g = (Graphics2D) g1.create();
		BasicStroke stroke = new BasicStroke(1.5f);
		g.setStroke(stroke);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		super.paintComponent(g);

		for (State s : states) {
			s.paintState(g);
		}
		for (Connection connection : connections) {
			g.setFont(new Font(null, Font.PLAIN, 17));
			for (Arrow arrow : connection.getArrows()) {
				arrow.paintArrow(g);
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getSize().width, getSize().height);
		super.paint(g);
	}

	private class MouseListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			setDelIndexesToNegative();
			lastPoint = e.getPoint();
			for (int index = states.size() - 1; index >= 0; index--) {
				foundSelectedElement = new Ellipse2D.Float(states.get(index).getX(), states.get(index).getY(), states.get(index).getDiameter(), states.get(index).getDiameter()).contains(lastPoint);
				if (foundSelectedElement) {
					selectedState = states.get(index);
					selectedState.setDraggingColor();
					repaint();
					break;
				}
			}

			if (!foundSelectedElement) {
				for (int index1 = 0; index1 < connections.size(); index1++) {
					ArrayList<Arrow> arrows = connections.get(index1).getArrows();
					for (int index2 = 0; index2 < arrows.size(); index2++) {
						Arrow arrow = arrows.get(index2);
						if (arrow instanceof NormalArrow) {
							AffineTransform nameTransform = ((NormalArrow) arrow).getNameTransform();
							AffineTransform nameTransformDuplicate = (AffineTransform) nameTransform.clone();
							try {
								nameTransformDuplicate.invert();
							} catch (NoninvertibleTransformException e1) {
								e1.printStackTrace();
							}
							Point2D namePoint = nameTransformDuplicate.transform(lastPoint, new Point2D.Double());
							if (((NormalArrow) arrow).getArrow().contains(lastPoint) || ((NormalArrow) arrow).getArrowCap().contains(lastPoint)
									|| ((NormalArrow) arrow).getNameBounds().contains(namePoint)) {
								connectionDelIndex = index1;
								arrowDelIndex = index2;
								foundSelectedElement = true;
								selectedArrow = arrow;
								((NormalArrow) arrow).setDraggingColor();
								repaint();
								break;
							}
						}
						if (arrow instanceof CurvedArrow) {
							AffineTransform arrowArcTransform = ((CurvedArrow) arrow).getCurveTransform();
							AffineTransform arrowArcDuplicate = (AffineTransform) arrowArcTransform.clone();
							AffineTransform nameTransform = ((CurvedArrow) arrow).getNameTransform();
							AffineTransform nameTransformDuplicate = (AffineTransform) nameTransform.clone();
							try {
								arrowArcDuplicate.invert();
								nameTransformDuplicate.invert();
							} catch (NoninvertibleTransformException e1) {
								e1.printStackTrace();
							}
							Point2D arcModPoint = arrowArcDuplicate.transform(lastPoint, new Point2D.Double());
							Point2D namePoint = nameTransformDuplicate.transform(lastPoint, new Point2D.Double());
							if (((CurvedArrow) arrow).getCurvedArrow().contains(arcModPoint) || ((CurvedArrow) arrow).getArrowCap().contains(lastPoint)
									|| ((CurvedArrow) arrow).getNameBounds().contains(namePoint)) {
								connectionDelIndex = index1;
								arrowDelIndex = index2;
								foundSelectedElement = true;
								selectedArrow = arrow;
								((CurvedArrow) arrow).setDraggingColor();
								repaint();
								break;
							}
						}
						if (arrow instanceof SelfArrow) {
							AffineTransform arrowArcTransform = ((SelfArrow) arrow).getArrowArcTransform();
							AffineTransform arrowArcDuplicate = (AffineTransform) arrowArcTransform.clone();
							AffineTransform arrowCapTransform = ((SelfArrow) arrow).getArrowCapTransform();
							AffineTransform arrowCapDuplicate = (AffineTransform) arrowCapTransform.clone();
							AffineTransform nameTransform = ((SelfArrow) arrow).getNameTransform();
							AffineTransform nameTransformDuplicate = (AffineTransform) nameTransform.clone();
							try {
								arrowArcDuplicate.invert();
								arrowCapDuplicate.invert();
								nameTransformDuplicate.invert();
							} catch (NoninvertibleTransformException e1) {
								e1.printStackTrace();
							}
							Point2D arcModPoint = arrowArcDuplicate.transform(lastPoint, new Point2D.Double());
							Point2D arcCapModPoint = arrowCapDuplicate.transform(lastPoint, new Point2D.Double());
							Point2D namePoint = nameTransformDuplicate.transform(lastPoint, new Point2D.Double());
							if (((SelfArrow) arrow).getArrowArc().contains(arcModPoint) || ((SelfArrow) arrow).getArrowCap().contains(arcCapModPoint)
									|| ((SelfArrow) arrow).getNameBounds().contains(namePoint)) {
								connectionDelIndex = index1;
								arrowDelIndex = index2;
								foundSelectedElement = true;
								selectedArrow = arrow;
								((SelfArrow) arrow).setDraggingColor();
								repaint();
								break;
							}
						}
					}
				}
			}
			if (previousSelectedState != null && selectedState == null) {
				previousSelectedState.setDefaultColor();
				repaint();
				return;
			}
			if (previousSelectedState != null && selectedState != null) {
				if (connectionClicked) {
					if (previousSelectedState.getX() == selectedState.getX() && previousSelectedState.getY() == selectedState.getY()) {
						if (connections.isEmpty()) {
							String conName = getConnectionNameFromInputFrame();
							if (!conName.equals("")) {
								Connection connection = new Connection(previousSelectedState, selectedState);
								connection.getArrows().add(new SelfArrow(previousSelectedState, selectedState, conName));
								connections.add(connection);
								previousSelectedState.setDefaultColor();
								selectedState = null;
								repaint();
								previousSelectedState = null;
								return;
							} else {
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							}
						} else {
							for (Connection con : connections) {
								if ((previousSelectedState.getX() == con.getState1().getX() && previousSelectedState.getY() == con.getState1().getY())
										&& (selectedState.getX() == con.getState2().getX() && selectedState.getY() == con.getState2().getY())) {
									if (con.getArrows().size() == 4) {
										JOptionPane.showMessageDialog(null, "Number of connections reached", "Error", JOptionPane.INFORMATION_MESSAGE);
										selectedState.setDefaultColor();
										repaint();
										selectedState = null;
										previousSelectedState = null;
										return;
									}
									addNeededSelfArrow(con.getArrows());
									previousSelectedState.setDefaultColor();
									selectedState = null;
									repaint();
									previousSelectedState = null;
									return;
								}
							}
							String conName = getConnectionNameFromInputFrame();
							if (!conName.equals("")) {
								Connection connection = new Connection(previousSelectedState, selectedState);
								connection.getArrows().add(new SelfArrow(previousSelectedState, selectedState, conName));
								connections.add(connection);
								previousSelectedState.setDefaultColor();
								selectedState = null;
								repaint();
								previousSelectedState = null;
								return;
							} else {
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							}
						}
					} else {
						if (connections.isEmpty()) {
							String conName = getConnectionNameFromInputFrame();
							if (!conName.equals("")) {
								Connection connection = new Connection(previousSelectedState, selectedState);
								connection.getArrows().add(new CurvedArrow(previousSelectedState, selectedState, conName, 10));
								connections.add(connection);
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							} else {
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							}
						} else {
							for (Connection con : connections) {
								if ((con.getState1().getX() != con.getState2().getX() || con.getState1().getY() != con.getState2().getY())) {
									if ((previousSelectedState.getX() == con.getState1().getX() && previousSelectedState.getY() == con.getState1().getY())
											|| (selectedState.getX() == con.getState1().getX() && selectedState.getY() == con.getState1().getY())) {
										if ((previousSelectedState.getX() == con.getState2().getX() && previousSelectedState.getY() == con.getState2().getY())
												|| (selectedState.getX() == con.getState2().getX() && selectedState.getY() == con.getState2().getY())) {
											if (con.getArrows().size() == 3) {
												JOptionPane.showMessageDialog(null, "Number of connections reached", "Error", JOptionPane.INFORMATION_MESSAGE);
												previousSelectedState.setDefaultColor();
												selectedState.setDefaultColor();
												repaint();
												selectedState = null;
												previousSelectedState = null;
												return;
											}
											addNeededArrow(con.getArrows());
											previousSelectedState.setDefaultColor();
											selectedState.setDefaultColor();
											repaint();
											selectedState = null;
											previousSelectedState = null;
											return;
										}
									}
								}
							}
							String conName = getConnectionNameFromInputFrame();


							if (!conName.equals("")) {
								Connection connection = new Connection(previousSelectedState, selectedState);
								connection.getArrows().add(new CurvedArrow(previousSelectedState, selectedState, conName, 10));
								connections.add(connection);
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							} else {
								previousSelectedState.setDefaultColor();
								selectedState.setDefaultColor();
								repaint();
								selectedState = null;
								previousSelectedState = null;
								return;
							}
						}
					}
				}
				if (!(previousSelectedState.getX() == selectedState.getX() && previousSelectedState.getY() == selectedState.getY())) {
					previousSelectedState.setDefaultColor();
					repaint();
					return;
				}
			}
			if (previousSelectedArrow != null && selectedArrow == null) {
				previousSelectedArrow.setDefaultColor();
				repaint();
				return;
			}
			if (previousSelectedArrow != null && selectedArrow != null) {
				if (previousSelectedArrow != null && !(checkArrowsForEquality(previousSelectedArrow, selectedArrow))) {
					previousSelectedArrow.setDefaultColor();
					repaint();
					return;
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			previousSelectedState = selectedState;
			stateDelIndex = states.indexOf(previousSelectedState);
			selectedState = null;
			foundSelectedElement = false;
			lastPoint = null;
			previousSelectedArrow = selectedArrow;
			selectedArrow = null;
			repaint();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if (selectedState == null) {
				return;
			}
			if (selectedState != null) {
				int dx = e.getX() - lastPoint.x;
				int dy = e.getY() - lastPoint.y;
				int resultX = ((selectedState.getX() + dx) / SNAP_GRID_INTERVAL) * SNAP_GRID_INTERVAL;
				int resultY = ((selectedState.getY() + dy) / SNAP_GRID_INTERVAL) * SNAP_GRID_INTERVAL;
				selectedState.setX(resultX);
				selectedState.setY(resultY);
				lastPoint = new Point(resultX + selectedState.getDiameter() / 2, resultY + selectedState.getDiameter() / 2);
				resizeFrameIfNeeded();
				selectedState.setDraggingColor();
				repaint();
			}
		}
	}

	private void resizeFrameIfNeeded() {
		if (getPreferredSize().width < selectedState.getX() + 150 && getPreferredSize().height > selectedState.getY() + 150) {
			setWindowSize(getPreferredSize().width + 150, getPreferredSize().height);
		} else {
			if (getPreferredSize().width > selectedState.getX() + 150 && getPreferredSize().height < selectedState.getY() + 150) {
				setWindowSize(getPreferredSize().width, getPreferredSize().height + 150);
			} else {
				if (getPreferredSize().width < selectedState.getX() + 150 && getPreferredSize().height < selectedState.getY() + 150) {
					setWindowSize(getPreferredSize().width + 150, getPreferredSize().height + 150);
				}
			}
		}
	}
	
	public void setWindowSize(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		frame.refreshFSMDiagScrollPane();
	}
	
	private class ScaleHandler implements MouseWheelListener {

		public void mouseWheelMoved(MouseWheelEvent e) {
			lastPoint = e.getPoint();
			if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
				for (int index = states.size() - 1; index >= 0; index--) {
					foundSelectedElement = new Ellipse2D.Float(states.get(index).getX(), states.get(index).getY(), states.get(index).getDiameter(), states.get(index).getDiameter())
							.contains(lastPoint);
					if (foundSelectedElement) {
						selectedState = states.get(index);
						break;
					}
				}
				if (selectedState != null) {
					int amount = e.getWheelRotation() * 2;
					selectedState.setDiameter(selectedState.getDiameter() + amount);
					selectedState.setDiameter(selectedState.getDiameter() + amount);
					repaint();
				}
			}
			foundSelectedElement = false;
			lastPoint = null;
			selectedState = null;
		}
	}


	private void removeAllStateConnections(State state) {
		for (int index = connections.size() - 1; index >= 0; index--) {
			if (connections.get(index).getState1().getName().equals(state.getName())) {
				connections.remove(index);
				continue;
			}
			if (connections.get(index).getState2().getName().equals(state.getName())) {
				connections.remove(index);
				continue;
			}
		}
	}

	public void addState() {
		InputFrame mini = new InputFrame("", "Add State");
		if (!mini.getInput().equals("")) {
			for (State state : states) {
				if (state.getName().equals(mini.getInput())) {
					JOptionPane.showMessageDialog(null, "A state with such a name already exists!", "Error", JOptionPane.INFORMATION_MESSAGE);
					addState();
					return;
				}
			}
			states.add(new State(0, 0, mini.getInput()));
			repaint();
		}
	}

	public void isConnectionClicked() {
		if (this.connectionClicked) {
			this.connectionClicked = false;
			return;
		}
		this.connectionClicked = true;
	}

	public void clearDiagram() {
		states.clear();
		connections.clear();
		repaint();
	}

	public void changeElementName() {
		if (previousSelectedState != null) {
			InputFrame mini = new InputFrame(previousSelectedState.getName(), "Edit State");
			String enteredName = mini.getInput().trim();
			if (!enteredName.equals("")) {
				for (State state : states) {
					if (previousSelectedState.getName().equals(state.getName())) {
						continue;
					}
					if (state.getName().equals(enteredName)) {
						JOptionPane.showMessageDialog(null, "Name already exists!", "Error", JOptionPane.INFORMATION_MESSAGE);
						changeElementName();
						return;
					}
				}
				previousSelectedState.setName(enteredName);
				previousSelectedState.setDefaultColor();
				repaint();
				previousSelectedState = null;
				return;
			}
		}
		if (previousSelectedArrow != null) {
			InputFrame mini = new InputFrame(previousSelectedArrow.getName(), "Edit Connection");
			if (!mini.getInput().trim().equals("")) {
				previousSelectedArrow.setName(mini.getInput());
				previousSelectedArrow.setDefaultColor();
				repaint();
				previousSelectedArrow = null;
				return;
			}
		}
	}
	
	private String getConnectionNameFromInputFrame() {
		InputFrame mini = new InputFrame("", "Add Connection");
		if (!mini.getInput().trim().equals("")) {
			return mini.getInput();
		}
		return "";
	}

	private void setDelIndexesToNegative() {
		stateDelIndex = -1;
		connectionDelIndex = -1;
		arrowDelIndex = -1;
	}

	private boolean checkArrowsForEquality(Arrow arrow1, Arrow arrow2) {
		if (arrow1.getStateFrom().getX() == arrow2.getStateFrom().getX() && arrow1.getStateFrom().getY() == arrow2.getStateFrom().getY()) {
			if (arrow1.getStateTo().getX() == arrow2.getStateTo().getX() && arrow1.getStateTo().getY() == arrow2.getStateTo().getY()) {
				if (arrow1 instanceof CurvedArrow && arrow2 instanceof CurvedArrow) {
					if (((CurvedArrow) arrow1).getCurveAngle() == ((CurvedArrow) arrow2).getCurveAngle()) {
						return true;
					}
				}
				if (arrow1 instanceof NormalArrow && arrow2 instanceof NormalArrow) {
					return true;
				}
				if (arrow1 instanceof SelfArrow && arrow2 instanceof SelfArrow) {
					if (((SelfArrow) arrow1).getAngle() == ((SelfArrow) arrow2).getAngle()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void addNeededSelfArrow(ArrayList<Arrow> arrows) {
		int fortyFive = -1, oneHundredThirtyFive = -1, twoHundredTwentyFive = -1, threeHundredFifteen = -1;
		for (Arrow arrow : arrows) {
			SelfArrow selfArrow = (SelfArrow) arrow;
			if (fortyFive == -1 && selfArrow.getAngle() == 45) {
				fortyFive = 1;
				continue;
			}
			if (oneHundredThirtyFive == -1 && selfArrow.getAngle() == 135) {
				oneHundredThirtyFive = 1;
				continue;
			}
			if (twoHundredTwentyFive == -1 && selfArrow.getAngle() == 225) {
				twoHundredTwentyFive = 1;
				continue;
			}
			if (threeHundredFifteen == -1 && selfArrow.getAngle() == 315) {
				threeHundredFifteen = 1;
				continue;
			}
		}
		String conName = getConnectionNameFromInputFrame();
		if (!conName.equals("")) {
			if (fortyFive == -1) {
				arrows.add(new SelfArrow(previousSelectedState, selectedState, conName));
				return;
			}
			if (oneHundredThirtyFive == -1) {
				SelfArrow self = new SelfArrow(previousSelectedState, selectedState, conName);
				self.setAngle(135);
				arrows.add(self);
				return;
			}
			if (twoHundredTwentyFive == -1) {
				SelfArrow self = new SelfArrow(previousSelectedState, selectedState, conName);
				self.setAngle(225);
				arrows.add(self);
				return;
			}
			if (threeHundredFifteen == -1) {
				SelfArrow self = new SelfArrow(previousSelectedState, selectedState, conName);
				self.setAngle(315);
				arrows.add(self);
				return;
			}
		}
	}

	private void addNeededArrow(ArrayList<Arrow> arrows) {
		int counterOfCurvedArrows = 0;
		CurvedArrow lastCurvedArrow = null;
		boolean fromStateEqualsArrowFromState = false;
		for (Arrow arrow : arrows) {
			if (arrow instanceof CurvedArrow) {
				counterOfCurvedArrows++;
				lastCurvedArrow = (CurvedArrow) arrow;
			}
		}
		String conName = getConnectionNameFromInputFrame();
		if (!conName.equals("")) {
			if (counterOfCurvedArrows == 0) {
				arrows.add(new CurvedArrow(previousSelectedState, selectedState, conName, 10));
				return;
			}
			if (counterOfCurvedArrows == 2) {
				arrows.add(new NormalArrow(previousSelectedState, selectedState, conName));
				return;
			}
			if (counterOfCurvedArrows == 1) {
				if (previousSelectedState.getX() == lastCurvedArrow.getStateFrom().getX() && previousSelectedState.getY() == lastCurvedArrow.getStateFrom().getY()) {
					fromStateEqualsArrowFromState = true;
				}
				if (fromStateEqualsArrowFromState) {
					if (lastCurvedArrow.getCurveAngle() == -10) {
						arrows.add(new CurvedArrow(previousSelectedState, selectedState, conName, 10));
					} else {
						arrows.add(new CurvedArrow(previousSelectedState, selectedState, conName, -10));
					}
				} else {
					if (lastCurvedArrow.getCurveAngle() == -10) {
						arrows.add(new CurvedArrow(previousSelectedState, selectedState, conName, -10));
					} else {
						arrows.add(new CurvedArrow(previousSelectedState, selectedState, conName, 10));
					}
				}
			}
		}
	}

	public ArrayList<State> getStates() {
		return states;
	}

	public ArrayList<Connection> getConnections() {
		return connections;
	}

	public void setStates(ArrayList<State> states) {
		this.states = states;
	}

	public void setConnections(ArrayList<Connection> connections) {
		this.connections = connections;
	}
	
	class KeyDispatcher implements KeyEventDispatcher {

		@Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_DELETE) {
				if (stateDelIndex != -1) {
					State delState = states.get(stateDelIndex);
					removeAllStateConnections(delState);
					states.remove(stateDelIndex);
					stateDelIndex = -1;
					previousSelectedState = null;
					selectedState = null;
					repaint();
					return true;
				}
				if (arrowDelIndex != -1) {
					connections.get(connectionDelIndex).getArrows().remove(arrowDelIndex);
					arrowDelIndex = -1;
					connectionDelIndex = -1;
					repaint();
					return true;
				}
			}
			return false;
		}
	}
}
