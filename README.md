# MSC/FSM diagram Buddy

## Important Notes
The application has been developed on Java 8 version 131. This guide assumes you have the following already installed.

- **Java** version *1.8.0_271* or *1.8.0_131*
- **Java JDK** version *1.8.0_131*
- **Java SE Runtime Enviroment** verison *1.8.0_271* or *1.8.0_131*

The versions above match the development environment in which this software product was built, feel free to use newer versions if they work.

## Compile and Run

1. Clone the repository
2. Open terminal
3. Direct to the src folder of the project
4. Run the following commads (for Windows):
 ```
    1. javac fsm\*.java
    2. javac msc\*.java
    3. jar cmvf META-INF\MANIFEST.MF msc.jar .
    4. java -jar msc.jar
```

## Syntax Guide (MSC tool)

### Formal Syntax

1. **title**  *"Diagram Title"*

2. *"Name of the Node"*  **->**  *"Name of another Node"*  **:**  *"Name of the singnal"*
3. *"Name of the Node"*  **x->**  *"Name of another Node"*  **:**  *"Name of the singnal"*
4. *"Name of the Node"*  **-->**  *"Name of another Node"*  **:**  *"Name of the singnal"*
5. *"Name of the Node"*  **x-->**  *"Name of another Node"*  **:**  *"Name of the singnal"*
6. *"Name of the Node"*  **->**  *"Name of the same Node"*  **:**  *"Name of the singnal"*

7. **note right of**  *"Name of the Node"*  **:**  *"Name of the Note"*
8. **note left of**  *"Name of the Node"*  **:**  *"Name of the Note"*
9. **note over**  *"Name of the Node"*  **:**  *"Name of the Note"*
10. **note over**  *"Name of the Node"* **,**  *"Name of another Node"*  **:**  *"Name of the Note"*

11. **activate**  *"Name of the Node"*
12. **deactivate**  *"Name of the Node"*
13. *"Name of the Node"*  **->  +** *"Name of another Node"* **:**  *"Name of the singnal"*
14. *"Name of the Node"*  **->  -** *"Name of another Node"*  **:**  *"Name of the singnal"*

### Example

1.	**title MSC**
2.	**Device -> Server : Connection Request**
3.	**Device x-> Server : Connection Request**
4.	**Server --> Device : Connection Accepted**
5.	**Device x--> Server : Connection Request**
6.	**Device -> Device : Restart**
7.	**note right of Device : Waiting**
8.	**note left of Device : Waiting**
9.	**note over Device : Waiting**
10.	 **note over Device, Server : Waiting**
11.	 **activate Device**
12.	 **deactivate Device**
13.	 **Server -> +Device**
14.	 **Server -> -Device : Deactivate**
